//
//  CDBGAddView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/25/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGAddView.h"

@implementation CDBGAddView

@synthesize drinkManager, scrollView, activeTextField, dismissKeyboardGesture;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        [self drawView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame drinkManager:(CDBGDrinkManager *)dm {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        drinkManager = dm;
        pickerListArray = [drinkManager getDrinkCategoryList];
        pickerIndex = 0;
        activeTextField = nil;
        [self drawView];
    }
    return self;
}

-(void)drawView {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat textAreaHeight = height * 0.08f;
    CGFloat textAreaWidth = width * 0.3f;
    CGFloat textFieldHeight = height * 0.06f;
    CGFloat buttonHeight = height * 0.06f;
    CGFloat buttonWidth = width * 0.2f;
    CGFloat yLocation = height * 0.08f;
    CGFloat verticalBuffer = height * 0.04f;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SCROLLVIEW
    CGFloat scrollViewX = 0;
    CGFloat scrollViewY = 0;
    CGFloat scrollViewW = width;
    CGFloat scrollViewH = height;
    scrollView= [[UIScrollView alloc] initWithFrame:CGRectMake(scrollViewX, scrollViewY, scrollViewW, scrollViewH)];
    [self addSubview:scrollView ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKNAMEAREA
    CGFloat drinkNameAreaW = textAreaWidth * 3.0f;
    CGFloat drinkNameAreaH = textAreaHeight;
    CGFloat drinkNameAreaX = (width - drinkNameAreaW) * 0.5f;
    CGFloat drinkNameAreaY = yLocation;
    drinkNameArea= [[UIView alloc] initWithFrame:CGRectMake(drinkNameAreaX, drinkNameAreaY, drinkNameAreaW, drinkNameAreaH)];
    drinkNameArea.layer.cornerRadius = drinkNameAreaH/8.0f;
    [drinkNameArea setBackgroundColor:[UIColor whiteColor]];
    drinkNameArea.layer.borderColor = UIColorFromRGB(0x1886FB).CGColor;
    drinkNameArea.layer.borderWidth = 3.0f;
    [scrollView addSubview:drinkNameArea ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKNAMEFIELD
    CGFloat drinkNameFieldX = drinkNameAreaW*.03f;
    CGFloat drinkNameFieldY = drinkNameAreaH*.11f;
    CGFloat drinkNameFieldW = drinkNameAreaW - (drinkNameFieldX *2.0f);
    CGFloat drinkNameFieldH = drinkNameAreaH -(drinkNameFieldY* 2.0f);
    drinkNameField = [[UITextField alloc] initWithFrame:CGRectMake(drinkNameFieldX, drinkNameFieldY, drinkNameFieldW, drinkNameFieldH)];
    drinkNameField.tag = 1;
    drinkNameField.layer.cornerRadius = drinkNameFieldH/8.0f;
    [drinkNameField setBackgroundColor:[UIColor whiteColor]];
    [drinkNameField setPlaceholder:@"Type New Drink Name"];
    [drinkNameField setFont:[UIFont fontWithName:@"GothamHTF-Book" size:drinkNameFieldH *.32f]];
    drinkNameField.delegate = self;
    [drinkNameArea addSubview:drinkNameField ];
    
    yLocation += drinkNameAreaH + verticalBuffer;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKCATEGORYSEGMENTEDCONTROL
    CGFloat drinkCategorySegmentedControlW = width * 0.9f;
    CGFloat drinkCategorySegmentedControlH = textFieldHeight;
    CGFloat drinkCategorySegmentedControlX = (width - drinkCategorySegmentedControlW) * 0.5f;
    CGFloat drinkCategorySegmentedControlY = yLocation;
    drinkCategorySegmentedControl = [[UISegmentedControl alloc] initWithItems:pickerListArray];
    drinkCategorySegmentedControl.frame = CGRectMake(drinkCategorySegmentedControlX, drinkCategorySegmentedControlY, drinkCategorySegmentedControlW, drinkCategorySegmentedControlH);
    [drinkCategorySegmentedControl addTarget:self action:@selector(drinkCategoryControlAction:) forControlEvents: UIControlEventValueChanged];
    drinkCategorySegmentedControl.selectedSegmentIndex = 0;
    [scrollView addSubview:drinkCategorySegmentedControl ];
    
    yLocation += drinkCategorySegmentedControlH + (verticalBuffer * 2.0f);
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE CALORIESLABEL
    CGFloat caloriesLabelW = textAreaWidth;
    CGFloat caloriesLabelH = textAreaHeight;
    CGFloat caloriesLabelX = (width - (caloriesLabelW * 2.0f)) / 2.0f;
    CGFloat caloriesLabelY = yLocation;
    caloriesLabel= [[UILabel alloc] initWithFrame:CGRectMake(caloriesLabelX, caloriesLabelY, caloriesLabelW, caloriesLabelH)];
    [caloriesLabel setText:@"Calories:"];
    [caloriesLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:caloriesLabelH ]];
    [caloriesLabel setTextAlignment:NSTextAlignmentLeft];
    [caloriesLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [scrollView addSubview:caloriesLabel ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE CALORIESFIELD
    CGFloat caloriesFieldW = textAreaWidth;
    CGFloat caloriesFieldH = textAreaHeight;
    CGFloat caloriesFieldX = width - caloriesFieldW - ((width - (caloriesFieldW * 2.0f)) / 2.0f);
    CGFloat caloriesFieldY = yLocation;
    caloriesField = [[UITextField alloc] initWithFrame:CGRectMake(caloriesFieldX, caloriesFieldY, caloriesFieldW, caloriesFieldH)];
    caloriesField.tag = 2;
    caloriesField.layer.cornerRadius = caloriesFieldH/8.0f;
    [caloriesField setBackgroundColor:[UIColor whiteColor]];
    caloriesField.layer.borderColor = UIColorFromRGB(0x1886FB).CGColor;
    caloriesField.layer.borderWidth = 2.0f;
    [caloriesField setFont:[UIFont fontWithName:@"GothamHTF-Book" size:caloriesFieldH *.32f]];
    caloriesField.delegate = self;
    caloriesField.textAlignment = UITextAlignmentCenter;
    caloriesField.keyboardType = UIKeyboardTypeDecimalPad;
    [scrollView addSubview:caloriesField ];
    
    yLocation += caloriesFieldH + verticalBuffer;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE OUNCESLABEL
    CGFloat ouncesLabelW = textAreaWidth;
    CGFloat ouncesLabelH = textAreaHeight;
    CGFloat ouncesLabelX = (width - (ouncesLabelW * 2.0f)) / 2.0f;
    CGFloat ouncesLabelY = yLocation;
    ouncesLabel= [[UILabel alloc] initWithFrame:CGRectMake(ouncesLabelX, ouncesLabelY, ouncesLabelW, ouncesLabelH)];
    [ouncesLabel setText:@"Ounces:"];
    [ouncesLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:ouncesLabelH ]];
    [ouncesLabel setTextAlignment:NSTextAlignmentLeft];
    [ouncesLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [scrollView addSubview:ouncesLabel ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE OUNCESFIELD
    CGFloat ouncesFieldW = textAreaWidth;
    CGFloat ouncesFieldH = textAreaHeight;
    CGFloat ouncesFieldX = width - ouncesFieldW - ((width - (ouncesFieldW * 2.0f)) / 2.0f);
    CGFloat ouncesFieldY = yLocation;
    ouncesField = [[UITextField alloc] initWithFrame:CGRectMake(ouncesFieldX, ouncesFieldY, ouncesFieldW, ouncesFieldH)];
    ouncesField.tag = 3;
    ouncesField.layer.cornerRadius = ouncesFieldH/8.0f;
    [ouncesField setBackgroundColor:[UIColor whiteColor]];
    ouncesField.layer.borderColor = UIColorFromRGB(0x1886FB).CGColor;
    ouncesField.layer.borderWidth = 2.0f;
    [ouncesField setFont:[UIFont fontWithName:@"GothamHTF-Book" size:ouncesFieldH *.32f]];
    ouncesField.delegate = self;
    ouncesField.textAlignment = UITextAlignmentCenter;
    ouncesField.keyboardType = UIKeyboardTypeDecimalPad;
    [scrollView addSubview:ouncesField ];
    
    yLocation += ouncesFieldH + verticalBuffer;
    
    if (drinkManager.drinkType != nonAlcoholic) {
        ////////////////////////////////////////////////////////////////////////////////
        // DRAWS THE alcoholPctLABEL
        CGFloat alcoholPctLabelW = textAreaWidth;
        CGFloat alcoholPctLabelH = textAreaHeight;
        CGFloat alcoholPctLabelX = (width - (alcoholPctLabelW * 2.0f)) / 2.0f;
        CGFloat alcoholPctLabelY = yLocation;
        alcoholPctLabel= [[UILabel alloc] initWithFrame:CGRectMake(alcoholPctLabelX, alcoholPctLabelY, alcoholPctLabelW, alcoholPctLabelH)];
        [alcoholPctLabel setText:@"Alcohol %:"];
        [alcoholPctLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:alcoholPctLabelH ]];
        [alcoholPctLabel setTextAlignment:NSTextAlignmentLeft];
        [alcoholPctLabel setTextColor:UIColorFromRGB(0x1886FB)];
        [scrollView addSubview:alcoholPctLabel ];
        
        ////////////////////////////////////////////////////////////////////////////////
        // DRAWS THE alcoholPctFIELD
        CGFloat alcoholPctFieldW = textAreaWidth;
        CGFloat alcoholPctFieldH = textAreaHeight;
        CGFloat alcoholPctFieldX = width - alcoholPctFieldW - ((width - (alcoholPctFieldW * 2.0f)) / 2.0f);
        CGFloat alcoholPctFieldY = yLocation;
        alcoholPctField = [[UITextField alloc] initWithFrame:CGRectMake(alcoholPctFieldX, alcoholPctFieldY, alcoholPctFieldW, alcoholPctFieldH)];
        alcoholPctField.tag = 4;
        alcoholPctField.layer.cornerRadius = alcoholPctFieldH/8.0f;
        [alcoholPctField setBackgroundColor:[UIColor whiteColor]];
        alcoholPctField.layer.borderColor = UIColorFromRGB(0x1886FB).CGColor;
        alcoholPctField.layer.borderWidth = 2.0f;
        [alcoholPctField setFont:[UIFont fontWithName:@"GothamHTF-Book" size:alcoholPctFieldH *.32f]];
        alcoholPctField.delegate = self;
        alcoholPctField.textAlignment = UITextAlignmentCenter;
        alcoholPctField.keyboardType = UIKeyboardTypeDecimalPad;
        [scrollView addSubview:alcoholPctField ];
        
        yLocation += alcoholPctFieldH + verticalBuffer;
    }
    
    yLocation += verticalBuffer;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE CREATEBUTTON
    CGFloat createButtonW = buttonWidth;
    CGFloat createButtonH = buttonHeight;
    CGFloat createButtonX = (width - (buttonWidth * 2.0f)) / 3.0f;
    CGFloat createButtonY = yLocation;
    createButton= [[UIButton alloc] initWithFrame:CGRectMake(createButtonX, createButtonY, createButtonW, createButtonH)];
    [createButton addTarget:self action:@selector(createButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [createButton setTitle:@"Create" forState:UIControlStateNormal];
    [createButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [createButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    createButton.layer.cornerRadius = createButtonH/8.0f;
    [scrollView addSubview:createButton ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE CANCELBUTTON
    CGFloat cancelButtonW = buttonWidth;
    CGFloat cancelButtonH = buttonHeight;
    CGFloat cancelButtonX = width - buttonWidth - ((width - (buttonWidth * 2.0f)) / 3.0f);
    CGFloat cancelButtonY = yLocation;
    cancelButton= [[UIButton alloc] initWithFrame:CGRectMake(cancelButtonX, cancelButtonY, cancelButtonW, cancelButtonH)];
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    cancelButton.layer.cornerRadius =cancelButtonH/8.0f;
    [scrollView addSubview:cancelButton ];

    yLocation += createButtonH + verticalBuffer;
}

- (void)drinkCategoryControlAction:(UISegmentedControl *)segment {
    [self hideKeyboard];
    pickerIndex = segment.selectedSegmentIndex;
}

-(IBAction) createButtonClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(createAction:drinkType:calories:ounces:alcoholPct:)]) {
        [self.delegate createAction:drinkNameField.text drinkType:[NSNumber numberWithInt:pickerIndex] calories:[self getNumberFromString:caloriesField.text]  ounces:[self getNumberFromString:ouncesField.text] alcoholPct:[self getNumberFromString:alcoholPctField.text]];
    }
}

-(IBAction) cancelButtonClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelAction)]) {
        [self.delegate cancelAction];
    }
}

#pragma mark - Keyboard editing
-(void) hideKeyboard {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UIKeyboardWillHideNotification"
                                                        object: nil
                                                      userInfo:nil];
    [self endEditing:YES];
}

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

-(void)deregisterFromKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    //
    // If the user taps anywhere outside the field and or keyboard
    // remove the keyboard.
    dismissKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [scrollView addGestureRecognizer:dismissKeyboardGesture];
}

//
// Handles the scrolling
-(void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //
    // allows for resize assuming it does not know the keyboard size
    if(keyboardSize.height == 0){
        keyboardSize.height = self.frame.size.height/2.5; // roughly
    }
    
    //
    // allows for scrolling once the keyboard is shown
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,keyboardSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    
    CGFloat fieldHeight = activeTextField.frame.size.height;
    CGFloat fieldY = activeTextField.frame.origin.y;
    CGFloat fieldX = activeTextField.frame.origin.x;
    
    
    //
    // accounts for the overlayview whiteView that offsets the textfields
    // by the y value of whitefield
    CGPoint fieldOrigin = CGPointMake(fieldX, fieldHeight + fieldY + 64);
    
    //
    // current visible screen in the scroll view minus the keyboard's height
    CGRect visibleRect = self.frame;
    visibleRect.size.height -= keyboardSize.height;
    
    //
    // If the bottom of the text field is not visible move the content offset
    if (!CGRectContainsPoint(visibleRect, fieldOrigin)){
        CGPoint scrollPoint = CGPointMake(0.0, (fieldY + fieldHeight + 64) - visibleRect.size.height);
        
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

-(void)keyboardWillBeHidden:(NSNotification*)aNotification {
    //
    // resizing the visible space to account for the missing keyboard.
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    [scrollView removeGestureRecognizer:dismissKeyboardGesture];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    activeTextField = nil;
    [scrollView removeGestureRecognizer:dismissKeyboardGesture];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    int tag = textField.tag + 1;
    if (tag == 4 && drinkManager.drinkType == nonAlcoholic) {
        tag += 1;
    }
    tag = tag % 5;
    if (tag == 0) {
        tag = 1;
    }
    UIResponder* nextResponder = [textField.superview.superview viewWithTag:tag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    }
    return NO;
}
         
-(NSNumber*)getNumberFromString:(NSString *)text
{
    NSNumber *number;
    @try {
        NSString *numberString;
    
        NSScanner *scanner = [NSScanner scannerWithString:text];
        NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
        
        // Throw away characters before the first number.
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
        
        // Collect numbers.
        [scanner scanCharactersFromSet:numbers intoString:&numberString];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        number = [f numberFromString:numberString];
    }
    @catch (NSException *e) {
        return [NSNumber numberWithInt:0];
    }
    
    return number;
}

@end
