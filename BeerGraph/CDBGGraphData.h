//
//  CDBGGraphData.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/27/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <Foundation/Foundation.h>

// This will hold information about graphs that will be drawn on the Display View.
@interface CDBGGraphData : NSObject

@property (nonatomic, strong) NSString *graphName;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSDictionary *displayData;

-(id)init:name data:(NSArray *)graphData displayData:(NSDictionary *)graphDisplayData;
@end
