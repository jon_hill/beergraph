//
//  CDBGDisplayViewController.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/24/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGDisplayViewController.h"

@interface CDBGDisplayViewController ()

@end

@implementation CDBGDisplayViewController

@synthesize drinkManager, drinkName, graphDataList;

-(void)viewDidLoad {
    [super viewDidLoad];

    //self.edgesForExtendedLayout = UIRectEdgeNone;
    
    drinkInfo = [drinkManager getDrinkInfo:drinkName];
    drinkSettings = [drinkManager getDrinkSettings];
    settingsViewActive = NO;
    
    graphDataList = [[NSMutableArray alloc] init];
    [self createGraphData];
    
    [self drawDisplayView];
    
    CGFloat buttonHeight = displayViewH * 0.04f;
    CGFloat buttonWidth = displayViewW * 0.4f;
    CGFloat yPadding = displayViewH * 0.01f;
    NSMutableArray *categories = [drinkManager getDrinkCategoryList];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SETTINGSBUTTON
    CGRect frameimg = CGRectMake(15, 5, 25,25);
    settingsButton = [[UIButton alloc] initWithFrame:frameimg];
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"Settings.png"] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(settingsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton setShowsTouchWhenHighlighted:YES];
    
    settingsNavigationButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];
    self.navigationItem.rightBarButtonItem = settingsNavigationButton;

    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SETTINGSVIEW
    CGFloat settingsViewX = 0;
    CGFloat settingsViewY = self.navigationController.navigationBar.frame.size.height + self.navigationController.navigationBar.frame.origin.y;
    CGFloat settingsViewW = displayViewW;
    CGFloat settingsViewH = (categories.count * buttonHeight) + ((categories.count + 1) * yPadding);
    settingsView = [[CDBGFilterView alloc] initWithFrame:CGRectMake(settingsViewX, settingsViewY, settingsViewW, settingsViewH) x:settingsViewX y:settingsViewY w:settingsViewW h:settingsViewW buttonWidth:buttonWidth buttonHeight:buttonHeight yPadding:yPadding settings:drinkSettings categoryNames:[drinkManager getDrinkCategoryList]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)drawDisplayView {
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DISPLAYVIEW
    CGFloat displayViewX = 0;
    CGFloat displayViewY = 0;
    displayViewW = width;
    displayViewH = height;
    displayView = [[CDBGDisplayView alloc] initWithFrame:CGRectMake(displayViewX, displayViewY, displayViewW, displayViewH)
                drinkName:drinkName
            drinkInfoText:[self getDrinkInfoLabelText]
             drinkPctText:[self getCaloriesPerOunceLabelText]
           caloriePctText:[self getCaloriePctOfAllDrinks]
    caloriePctForTypeText:[self getCaloriePctOfThisDrinkType]
            graphDataList:graphDataList];
    [self.view addSubview:displayView];
}

-(IBAction)settingsButtonClicked:(id)sender {
    if (settingsViewActive == YES) {
        settingsViewActive = NO;
        // Save the drink settings
        [drinkManager saveDrinkSettings:drinkSettings];
        
        // Remove the settings view
        [settingsView removeFromSuperview];
        
        // Get new data
        [self createGraphData];
        [displayView updateView:graphDataList];
        
        // Move the displayView up
        displayView.frame = CGRectOffset(displayView.frame, 0.0, -settingsViewHeight);
        [displayView setNeedsDisplay];
        [self.view setNeedsDisplay];
    }
    else {
        settingsViewActive = YES;
        [self.view addSubview:settingsView];
        settingsViewHeight = settingsView.frame.size.height;
        // Move the displayView down
        displayView.frame = CGRectOffset(displayView.frame, 0.0, settingsViewHeight);
        [self.view setNeedsDisplay];
    }
}

#pragma mark - FilterViewActionProtocol

-(void)checkboxClickAction:(CDBGDrinkSettings *)ds {
    drinkSettings = ds;
}

-(NSString *)getDrinkInfoLabelText {
    // Set the drink info
    NSMutableString *textString = [NSMutableString stringWithFormat:@"%d cal, %d oz", [drinkInfo.calories intValue], [drinkInfo.ounces intValue]];
    if (![drinkInfo.alcohol_pct isEqual:@0]) {
        [textString appendString:[NSString stringWithFormat:@", %2.1f%% alcohol", [drinkInfo.alcohol_pct doubleValue]]];
    }
    return textString;
}

-(NSString *)getCaloriesPerOunceLabelText {
    double calories = [drinkInfo.calories doubleValue];
    double ounces = [drinkInfo.ounces doubleValue];
    double alcohol_pct = [drinkInfo.alcohol_pct doubleValue];
    double caloriesPerOunce = calories / ounces;
    
    NSString *text;
    
    if ([drinkInfo.alcohol_pct isEqual:@0]) {
        // Non-alcoholic drink
        text = [NSString stringWithFormat:@"%3.0f Calories per Ounce", caloriesPerOunce];
    }
    else {
        // Alcoholic drink
        caloriesPerOunce /= (alcohol_pct / 100);
        text = [NSString stringWithFormat:@"%3.0f Calories per Ounce of Alcohol", caloriesPerOunce];
    }
    return text;
}

-(NSString *)getCaloriePctOfAllDrinks {
    // Set the percent of all drinks calories per ounce label
    NSMutableArray *caloriesList = [drinkManager getAllDrinkCaloriesPerOunceForCategory:-1];
    
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [caloriesList sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    
    double cal = [drinkManager getCaloriesPerOunce:drinkName];
    // Determine where in the sorted list the current drink's calories is
    for (int i = 0; i < caloriesList.count; i++) {
        if (cal >= [caloriesList[i] doubleValue]) {
            int pct = ((double)(i + 1) / (double)caloriesList.count) * 100;
            return [NSString stringWithFormat:@"%d%% of all drinks", pct];
        }
    }
    return @"";
}

-(NSString *)getCaloriePctOfThisDrinkType {
    // Set the percent of this category of drinks calories per ounce label
    CDBGDrink *drink = drinkInfo.drink;
    NSMutableArray *caloriesList = [drinkManager getAllDrinkCaloriesPerOunceForCategory:[drink.category intValue]];
    
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [caloriesList sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    
    double cal = [drinkManager getCaloriesPerOunce:drinkName];
    // Determine where in the sorted list the current drink's calories is
    for (int i = 0; i < caloriesList.count; i++) {
        if (cal >= [caloriesList[i] doubleValue]) {
            int pct = ((double)(i + 1) / (double)caloriesList.count) * 100;
            
            return [NSString stringWithFormat:@"%d%% of all %@", pct, [drinkManager getDrinkCategory:drink.name]];
        }
    }
    return @"";
}

-(CDBGGraphData *)getCaloriesPerOunceGraphData {
    NSArray *caloriesList = [self getSortedCaloriesList];
    NSString *name = (drinkManager.drinkType == nonAlcoholic) ? @"Calories Per Ounce" : @"Calories Per Ounce of Alcohol";
    NSMutableDictionary *displayList = [[NSMutableDictionary alloc] init];
    [self addToDictionary:displayList drink:drinkName index:[drinkManager getCaloriesPerOunce:drinkName]];
    
    // Add the reference drinks
    if ([drinkSettings.filterTypical boolValue] == YES) {
        NSArray *references = [drinkManager getReferenceDrinks];
        for (CDBGDrink *drink in references) {
            [self addToDictionary:displayList drink:drink.name index:[drinkManager getCaloriesPerOunce:drink.name]];
        }
    }
    
    // Add the favorites
    if ([drinkSettings.filterFavorites boolValue] == YES) {
        NSArray *favoritesList = [drinkManager getFavorites];
        for (CDBGDrink *drink in favoritesList) {
            [self addToDictionary:displayList drink:drink.name index:[drinkManager getCaloriesPerOunce:drink.name]];
        }
    }
    
    return [[CDBGGraphData alloc] init:name data:caloriesList displayData:displayList];
}

-(NSArray *)getSortedCaloriesList {
    NSMutableArray *caloriesList = [[NSMutableArray alloc] init];

    // Get the category of the selected drink
    int cat = [[drinkManager getCategoryNumber:[drinkManager getDrinkCategory:drinkName]] intValue];
    NSArray *categorySelections = [drinkSettings makeFilterCategoriesArray:drinkSettings.filterCategoriesMask];
    for (int i = 0; i < categorySelections.count; i++) {
        bool selected = [categorySelections[i] boolValue];
        if (selected == YES) {
            [caloriesList addObjectsFromArray:[drinkManager getAllDrinkCaloriesPerOunceForCategory:i]];
        }
        else {
            // If the category settings would exclude the category that the
            // selected drink is in, make sure that the selected drink is
            // still included
            if (i == cat) {
                [caloriesList addObject:[NSNumber numberWithDouble:[drinkManager getCaloriesPerOunce:drinkName]]];
            }
        }
    }
    
    NSSortDescriptor *lowestToHighest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [caloriesList sortUsingDescriptors:[NSArray arrayWithObject:lowestToHighest]];
    
    return caloriesList;
}

-(CDBGGraphData *)getAlcoholPercentByVolumeGraphData {
    NSArray *alcoholPctList = [self getSortedAlcoholList];
    NSString *name = @"Alcohol % By Volume";
    NSMutableDictionary *displayList = [[NSMutableDictionary alloc] init];
    [self addToDictionary:displayList drink:drinkName index:[drinkManager getAlcoholPct:drinkName]];
    
    // Add the reference drinks
    if ([drinkSettings.filterTypical boolValue] == YES) {
        NSArray *typicalDrinks = [drinkManager getReferenceDrinks];
        for (CDBGDrink *drink in typicalDrinks) {
            [self addToDictionary:displayList drink:drink.name index:[drinkManager getAlcoholPct:drink.name]];
        }
    }
    
    // Add the favorites
    if ([drinkSettings.filterFavorites boolValue] == YES) {
        NSArray *favoriteDrinks = [drinkManager getFavorites];
        for (CDBGDrink *drink in favoriteDrinks) {
            [self addToDictionary:displayList drink:drink.name index:[drinkManager getAlcoholPct:drink.name]];
        }
    }
    
    return [[CDBGGraphData alloc] init:name data:alcoholPctList displayData:displayList];
}

-(NSArray *)getSortedAlcoholList {
    NSMutableArray *alcoholList = [[NSMutableArray alloc] init];
    
    // Get the category of the selected drink
    int cat = [[drinkManager getCategoryNumber:[drinkManager getDrinkCategory:drinkName]] intValue];

    NSArray *categorySelections = [drinkSettings makeFilterCategoriesArray:drinkSettings.filterCategoriesMask];
    for (int i = 0; i < categorySelections.count; i++) {
        bool selected = [categorySelections[i] boolValue];
        if (selected == YES) {
            [alcoholList addObjectsFromArray:[drinkManager getAlcoholPctForCategory:i]];
        }
        else {
            // If the category settings would exclude the category that the
            // selected drink is in, make sure that the selected drink is
            // still included
            if (i == cat) {
                [alcoholList addObject:[NSNumber numberWithDouble:[drinkManager getAlcoholPct:drinkName]]];
            }
        }
    }
    
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [alcoholList sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    
    return alcoholList;
}

-(void)addToDictionary:(NSMutableDictionary *)displayList drink:(NSString *)name index:(double)index {
    NSNumber *i = [NSNumber numberWithDouble:index];
    NSMutableArray *drinkArray = [displayList objectForKey:i];
    if (drinkArray == nil) {
        drinkArray = [[NSMutableArray alloc] init];
    }
    [drinkArray addObject:name];
    [displayList setObject:drinkArray forKey:i];
}

-(void)createGraphData {
    [graphDataList removeAllObjects];
    [graphDataList addObject:[self getCaloriesPerOunceGraphData]];
    [graphDataList addObject:[self getAlcoholPercentByVolumeGraphData]];
}

@end
