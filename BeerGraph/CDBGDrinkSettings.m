//
//  DrinkSettings.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/19/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGDrinkSettings.h"


@implementation CDBGDrinkSettings

@dynamic drinkType;
@dynamic filterTypical;
@dynamic filterFavorites;
@dynamic filterCategoriesMask;

-(NSString *)makeFilterCategoriesMask:(NSArray *)fc {
    NSMutableString *fcString = [[NSMutableString alloc] init];
    for (NSNumber *cat in fc) {
        [fcString appendString:([cat boolValue] == YES) ? @"1 " : @"0"];
    }
    return fcString;
}

-(NSArray *)makeFilterCategoriesArray:(NSString *)fc {
    NSArray *maskFields = [fc componentsSeparatedByString:@" "];
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    for (NSNumber *cat in maskFields) {
        [categories addObject:cat];
    }
    return categories;
}

@end
