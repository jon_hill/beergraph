//
//  HomeViewController.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/12/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGHomeView.h"

@interface CDBGHomeViewController : UIViewController <HomeViewActionProtocol> {
    CDBGHomeView *homeView;
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) CDBGDrinkManager *drinkManager;

- (void)selectAction;
- (void)addAction;
- (void)settingsAction;

@end
