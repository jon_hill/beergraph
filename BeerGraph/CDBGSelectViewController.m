//
//  SelectViewController.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/21/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGSelectViewController.h"
#import "CDBGDisplayViewController.h"

@interface CDBGSelectViewController ()

@end

@implementation CDBGSelectViewController

@synthesize drinkManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = @"Select Drink";
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SELECTVIEW
    CGFloat selectViewX = 0;
    CGFloat selectViewY = 0;
    CGFloat selectViewW = width;
    CGFloat selectViewH = height;
    selectView= [[CDBGSelectView alloc] initWithFrame:CGRectMake(selectViewX, selectViewY, selectViewW, selectViewH) drinkManager:(CDBGDrinkManager *)drinkManager];
    selectView.delegate = self;
    
    [self.view addSubview:selectView ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SelectViewActionProtocol

-(void)selectAction:(NSString *)drinkSelected {
    CDBGDisplayViewController *displayViewController = [[CDBGDisplayViewController alloc] init];
    displayViewController.drinkManager = self.drinkManager;
    displayViewController.drinkName = drinkSelected;
    [self.navigationController pushViewController:displayViewController animated:YES];
}

-(bool)isFavorite:(NSString *)drinkName {
    return [drinkManager isFavorite:drinkName];
}

-(bool)toggleFavorite:(NSString *)drinkName {
    return [drinkManager toggleFavorite:drinkName];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
