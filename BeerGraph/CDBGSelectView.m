//
//  CDBGSelectView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/23/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGSelectView.h"

@implementation CDBGSelectView

@synthesize drinkManager, drinkNames, filteredDrinks, searchBar, selectTableView;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        [self drawView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame drinkManager:(CDBGDrinkManager *)dm {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        drinkManager = dm;
        drinkNames = [drinkManager getDrinkNames];
        filteredDrinks = [drinkNames copy];
        [self drawView];
    }
    return self;
}

-(void)drawView {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat yLocation = 0;

    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SEARCHBAR
    CGFloat searchBarX = 0;
    CGFloat searchBarY = yLocation;
    CGFloat searchBarW = width;
    CGFloat searchBarH = height * 0.08f;
    searchBar= [[UISearchBar alloc] initWithFrame:CGRectMake(searchBarX, searchBarY, searchBarW, searchBarH)];
    searchBar.delegate = self;
    [self addSubview:searchBar ];
    
    yLocation += searchBarH;

    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SELECTTABLEVIEW
    CGFloat selectTableViewX = 0;
    CGFloat selectTableViewY = yLocation;
    CGFloat selectTableViewW = width;
    CGFloat selectTableViewH = height - yLocation;
    selectTableView= [[UITableView alloc] initWithFrame:CGRectMake(selectTableViewX, selectTableViewY, selectTableViewW, selectTableViewH)];
    selectTableView.dataSource = self;
    selectTableView.delegate = self;
    [self addSubview:selectTableView ];
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [filteredDrinks count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        /*
         *   Actually create a new cell (with an identifier so that it can be dequeued).
         */
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    UIImage *star = [UIImage imageNamed:@"Star.png"];
    CGFloat starWidth = star.size.width;
    CGFloat starHeight = star.size.height;
    CGFloat cellWidth = cell.bounds.size.width;
    CGFloat cellHeight = cell.bounds.size.height;
    CGRect buttonRect = CGRectMake(cellWidth - starWidth, (cellHeight - starHeight) * 0.5f, starWidth, starHeight);
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    bool isFavorite;
    button.frame = buttonRect;
    button.tag = indexPath.row;
    if (self.delegate && [self.delegate respondsToSelector:@selector(isFavorite:)]) {
        isFavorite = [self.delegate isFavorite:[filteredDrinks objectAtIndex:indexPath.row]];
    }
    if (isFavorite == YES) {
        [button setImage:[UIImage imageNamed:@"StarSelected.png"] forState:UIControlStateNormal];
    }
    else {
        [button setImage:[UIImage imageNamed:@"Star.png"] forState:UIControlStateNormal];
    }
    [button addTarget:self action:@selector(favoriteClicked:) forControlEvents:UIControlEventTouchUpInside];

    // Set up the cell...
    NSString *name = [filteredDrinks objectAtIndex:indexPath.row];
    NSLog(@"Drink Name: %@", name);
    cell.textLabel.text = name;
    cell.detailTextLabel.text = nil;
    [cell.contentView insertSubview:button atIndex:indexPath.row];
    
    return cell;
}

-(IBAction)favoriteClicked:(UIButton *)sender {
    bool isFavorite;
    if (self.delegate && [self.delegate respondsToSelector:@selector(toggleFavorite:)]) {
        isFavorite = [self.delegate toggleFavorite:[filteredDrinks objectAtIndex:sender.tag]];
    }
    if (isFavorite == YES) {
        [sender setImage:[UIImage imageNamed:@"StarSelected.png"] forState:UIControlStateNormal];
    }
    else {
        [sender setImage:[UIImage imageNamed:@"Star.png" ] forState:UIControlStateNormal];
    }
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectAction:)]) {
        [self.delegate selectAction:[filteredDrinks objectAtIndex:indexPath.row]];
    }
}

#pragma mark - UISearchButtonDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    filteredDrinks = [drinkNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF like[c]  %@", [searchText stringByAppendingString:@"*"]]];
    [self.selectTableView reloadData];
}

@end
