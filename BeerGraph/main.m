//
//  main.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/7/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDBGAppDelegate class]));
    }
}
