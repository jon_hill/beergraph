//
//  CDBGAddView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/25/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGConstants.h"
#import "CDBGAddView.h"
#import "CDBGDrinkManager.h"

@protocol AddViewActionProtocol <NSObject>
@required
-(void)createAction:(NSString *)drinkName drinkType:(NSNumber *)type calories:(NSNumber *)cal ounces:(NSNumber *)oz alcoholPct:(NSNumber *)alcPct;
-(void)cancelAction;

@end

@interface CDBGAddView : UIView <UIScrollViewDelegate, UIActionSheetDelegate, UITextFieldDelegate> {
    UISegmentedControl *drinkCategorySegmentedControl;
    NSArray *pickerListArray;
    UIView *drinkNameArea;
    UITextField *drinkNameField;
    UILabel *caloriesLabel;
    UITextField *caloriesField;
    UILabel *ouncesLabel;
    UITextField *ouncesField;
    UILabel *alcoholPctLabel;
    UITextField *alcoholPctField;
    UIButton *createButton;
    UIButton *cancelButton;
    int pickerIndex;
}

@property (nonatomic, weak) id<AddViewActionProtocol> delegate;
@property (nonatomic, strong) CDBGDrinkManager *drinkManager;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UITextField *activeTextField;
@property (nonatomic, strong) UITapGestureRecognizer* dismissKeyboardGesture;

-(id)initWithFrame:(CGRect)frame;
-(id)initWithFrame:(CGRect)frame drinkManager:(CDBGDrinkManager *)dm;
-(void)drawView;
-(void)drinkCategoryControlAction:(UISegmentedControl *)segment;
-(IBAction)createButtonClicked:(id)sender;
-(IBAction)cancelButtonClicked:(id)sender;
-(void)hideKeyboard;
-(void)registerForKeyboardNotifications;
-(void)deregisterFromKeyboardNotifications;
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)textFieldDidBeginEditing:(UITextField *)textField;
-(void)keyboardWasShown:(NSNotification *)notification;
-(void)keyboardWillBeHidden:(NSNotification*)aNotification;
-(void)textFieldDidEndEditing:(UITextField *)textField;
-(BOOL)textFieldShouldReturn:(UITextField *)textField;
-(NSNumber*)getNumberFromString:(NSString *)text;

@end
