//
//  CDBGSelectView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/23/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"

@protocol SelectViewActionProtocol <NSObject>
@required
-(void)selectAction:(NSString *)drinkSelected;
-(bool)isFavorite:(NSString *)drinkName;
-(bool)toggleFavorite:(NSString *)drinkName;

@end

@interface CDBGSelectView : UIView <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITableView *selectTableView;
}

@property (nonatomic, weak) id<SelectViewActionProtocol> delegate;
@property (nonatomic, strong) CDBGDrinkManager *drinkManager;
@property (nonatomic, strong) NSArray *drinkNames;  // Array of drink names
@property (nonatomic, strong) NSArray *filteredDrinks;  // Filtered array of drink names
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *selectTableView;

-(id)initWithFrame:(CGRect)frame;
-(id)initWithFrame:(CGRect)frame drinkManager:(CDBGDrinkManager *)dm;
-(void)drawView;
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
-(IBAction)favoriteClicked:(UIButton *)sender;
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;

@end
