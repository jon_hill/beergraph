//
//  CDBGSettingsViewController.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/24/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGSettingsView.h"

@interface CDBGSettingsViewController : UIViewController <SettingsViewActionProtocol, UIActionSheetDelegate> {
    CDBGSettingsView* settingsView;
}

@property (strong, nonatomic) CDBGDrinkManager *drinkManager;

-(void)drinkTypeSelectdAction:(NSNumber *)selection;
-(void)importDrinksAction;
-(void)deleteUserCreatedDrinksAction;
-(void)deleteAllDrinksAction;
-(void)confirmDialog:(NSString *)text tag:(int)tag;
-(void)drinkStatusDialog:(NSString *)text tag:(int)tag;
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;

@end
