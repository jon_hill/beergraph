//
//  SelectViewController.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/21/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGSelectView.h"

@interface CDBGSelectViewController : UIViewController <SelectViewActionProtocol> {
    CDBGSelectView* selectView;
}

@property (nonatomic, strong) CDBGDrinkManager *drinkManager;

-(void)selectAction:(NSString *)drinkSelected;
-(bool)isFavorite:(NSString *)drinkName;
-(bool)toggleFavorite:(NSString *)drinkName;

@end
