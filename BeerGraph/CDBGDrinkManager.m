//
//  DrinkManager.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/13/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGDrinkManager.h"

@implementation CDBGDrinkManager

@synthesize drinkType, filterTypical, filterFavorites, filterCategories, managedObjectContext, alcoholicCategoryList, nonalcoholicCategoryList;

-(id)init:(NSManagedObjectContext *)moc{
    self = [super init];
    if (self) {
        self.managedObjectContext = moc;

       // Try to read the drink type from the settings in the database
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDBGDrinkSettings" inManagedObjectContext:managedObjectContext];
        [fetchRequest setEntity:entity];
        NSError *error;
        NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];

        if (fetchedObjects.count == 0) {
            // Default to alcoholic
            drinkType = alcoholic;
        }
        else {
            CDBGDrinkSettings *drinkSettings = fetchedObjects[0];
            drinkType = [drinkSettings.drinkType intValue];
        }
        
        alcoholicCategoryList = @[@"beer", @"wine", @"hard", @"mixed"];
        nonalcoholicCategoryList = @[@"soda", @"water", @"juice", @"non-alcoholic beer"];
    }
    return self;
}

-(NSArray *)getDrinks {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDBGDrink"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    // Filter drinks based on drinkType
    if (drinkType != allDrinks) {
        NSMutableArray *filteredDrinks = [[NSMutableArray alloc] init];
        for (CDBGDrink *drink in fetchedObjects) {
            CDBGDrinkInfo *drinkInfo = drink.info;
            if ([drinkInfo.alcohol_pct isEqual:@0]) {
                if (drinkType == nonAlcoholic) {
                    [filteredDrinks addObject:drink];
                }
            }
            else {
                if (drinkType == alcoholic) {
                    [filteredDrinks addObject:drink];
                }
            }
        }
        fetchedObjects = [filteredDrinks copy];
    }
    return fetchedObjects;
}

-(NSArray *)getDrinkNames {
    NSArray *drinks = [self getDrinks];
    NSMutableArray *drinkNameList = [[NSMutableArray alloc] init];
    for (CDBGDrink *drink in drinks) {
        [drinkNameList addObject:drink.name];
    }
    return (NSArray *)[drinkNameList sortedArrayUsingSelector:@selector(compare:)];
}

-(BOOL)doesDrinkExist:(NSString *)name {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    NSArray *filteredArray = [[self getDrinks] filteredArrayUsingPredicate:predicate];
    return ([filteredArray count] > 0);
}

-(NSString *)getDrinkCategory:(NSString *)name {
    CDBGDrink *drink = [self getDrink:name];
    
    if (drink == nil)
        return nil;
    
    int index = [drink.category intValue];
    CDBGDrinkInfo *drinkInfo = drink.info;
    if ([drinkInfo.alcohol_pct isEqual:@0]) {
        return nonalcoholicCategoryList[index];
    }
    else {
        return alcoholicCategoryList[index];
    }
    return nil;
}

-(NSNumber *)getCategoryNumber:(NSString *)category {
    for (int index = 0; index < alcoholicCategoryList.count; index++) {
        if ([category isEqualToString:alcoholicCategoryList[index]]) {
            return [NSNumber numberWithInt:index];
        }
    }
    
    for (int index = 0; index < nonalcoholicCategoryList.count; index++) {
        if ([category isEqualToString:nonalcoholicCategoryList[index]]) {
            return [NSNumber numberWithInt:index];
        }
    }
    
    return [NSNumber numberWithInt:100];
}

-(NSError *)saveDrink:(NSString *)drinkName cal:(NSNumber *)calories oz:(NSNumber *)ounces alc:(NSNumber *)alcohol_pct cat:(NSNumber *)category fav:(NSNumber *)favorite ref:(NSNumber *)reference {
    CDBGDrinkInfo *drinkInfo = [NSEntityDescription
                            insertNewObjectForEntityForName:@"CDBGDrinkInfo"
                            inManagedObjectContext:self.managedObjectContext];
    CDBGDrink *drink = [NSEntityDescription
                    insertNewObjectForEntityForName:@"CDBGDrink"
                    inManagedObjectContext:self.managedObjectContext];
    
    drinkInfo.calories = calories;
    drinkInfo.ounces = ounces;
    drinkInfo.alcohol_pct = alcohol_pct;
    drinkInfo.drink = drink;
    
    drink.name = drinkName;
    drink.locked = [NSNumber numberWithBool:NO];
    drink.category = category;
    drink.favorite = favorite;
    drink.reference = reference;
    drink.info = drinkInfo;
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    return error;
}

-(NSError *)deleteDrink:(NSString *)name {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    NSArray *filteredArray = [[self getDrinks] filteredArrayUsingPredicate:predicate];
    if ([filteredArray count] > 0) {
        [managedObjectContext deleteObject:filteredArray[0]];
    }
    return nil;
}

-(NSError *)deleteDrinks:(DeleteSelection)selection {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDBGDrink"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (CDBGDrink *drink in fetchedObjects) {
        if ([drink.locked boolValue] == YES && selection != unlocked) {
            [managedObjectContext deleteObject:drink];
        }
        else if ([drink.locked boolValue] == NO && selection != locked) {
            [managedObjectContext deleteObject:drink];
        }
    }
    // Update the drinkList
    [self getDrinks];
    [self getDrinkNames];

    return nil;
}

-(CDBGDrink *)getDrink:(NSString *)name {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    NSArray *filteredArray = [[self getDrinks] filteredArrayUsingPredicate:predicate];
    if ([filteredArray count] > 0) {
        return (CDBGDrink *)filteredArray[0];
    }
    return nil;
}

-(CDBGDrinkInfo *)getDrinkInfo:(NSString *)name {
    CDBGDrink *drink = [self getDrink:name];
    if (drink != nil) {
        return (CDBGDrinkInfo *)drink.info;
    }
    return nil;
}

-(double)getCaloriesPerOunce:(NSString *)name {
    CDBGDrinkInfo *drinkInfo = [self getDrinkInfo:name];
    
    double calories = [drinkInfo.calories doubleValue];
    double ounces = [drinkInfo.ounces doubleValue];
    double alcohol_pct = [drinkInfo.alcohol_pct doubleValue];
    double caloriesPerOunce = calories / ounces;
    
    if (![drinkInfo.alcohol_pct isEqual:@0]) {
        // Alcoholic drink
        caloriesPerOunce /= (alcohol_pct / 100);
    }
    return caloriesPerOunce;
}

-(NSMutableArray *)getAllDrinkCaloriesPerOunceForCategory:(int)category {
    // Use category to filter the drinks per category.
    // if category == -1, return all drinks
    // Return the Calories Per Ounce of Alcohol for alcoholic drinks.
    // Return the Calories Per Ounce for non-alcoholic drinks.
    NSMutableArray *caloriesList = [[NSMutableArray alloc] init];
    for (CDBGDrink *drink in [self getDrinks]) {
        CDBGDrinkInfo *drinkInfo = drink.info;
        int cat = [drink.category intValue];
        if (category != -1 && cat != category) {
            continue;
        }
        double calories = [drinkInfo.calories doubleValue];
        double ounces = [drinkInfo.ounces doubleValue];
        double alcohol_pct = [drinkInfo.alcohol_pct doubleValue];
        double caloriesPerOunce = calories / ounces;

        if ([drinkInfo.alcohol_pct isEqual:@0]) {
            // Non-alcoholic drink
            if (drinkType == nonAlcoholic) {
                [caloriesList addObject:[NSNumber numberWithDouble:caloriesPerOunce]];
            }
        }
        else {
            // Alcoholic drink
            if (drinkType == alcoholic) {
                caloriesPerOunce /= (alcohol_pct / 100);
                [caloriesList addObject:[NSNumber numberWithDouble:caloriesPerOunce]];
            }
        }
    }
    return caloriesList;
}

-(void)importDrinkList {
    // Delete all the locked entries in the database
    [self deleteDrinks:locked];
    
    // Update the drinkList
    [self getDrinks];
    [self getDrinkNames];

    // Read in the new list
    NSString *plistCatPath = [[NSBundle mainBundle] pathForResource:@"DrinkList" ofType:@"plist"];
    NSMutableArray *tempDrinkList = [NSMutableArray arrayWithContentsOfFile:plistCatPath];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    for (int drinkIndex = 0; drinkIndex < tempDrinkList.count; drinkIndex++) {
        // Check to see if the current drink we are importing matches the name of a user defined drink.
        CDBGDrink *existingDrink = [self getDrink:tempDrinkList[drinkIndex][@"name"]];
        if (existingDrink != nil) {
            // If the drink exists, delete it
            [self deleteDrink:existingDrink.name];
        }
        CDBGDrinkInfo *drinkInfo = [NSEntityDescription
                                insertNewObjectForEntityForName:@"CDBGDrinkInfo"
                                inManagedObjectContext:self.managedObjectContext];
        CDBGDrink *drink = [NSEntityDescription
                        insertNewObjectForEntityForName:@"CDBGDrink"
                        inManagedObjectContext:self.managedObjectContext];
        NSString *numberString = tempDrinkList[drinkIndex][@"calories"];
        NSNumber *number = [f numberFromString:numberString];
        drinkInfo.calories = number;
        numberString = tempDrinkList[drinkIndex][@"ounces"];
        number = [f numberFromString:numberString];
        drinkInfo.ounces = number;
        numberString = tempDrinkList[drinkIndex][@"alcohol_pct"];
        number = [f numberFromString:numberString];
        drinkInfo.alcohol_pct = number;
        drinkInfo.drink = drink;
        
        drink.name = tempDrinkList[drinkIndex][@"name"];
        drink.locked = [NSNumber numberWithBool:YES];
        numberString = tempDrinkList[drinkIndex][@"category"];
        number = [self getCategoryNumber:numberString];
        drink.category = number;
        numberString = tempDrinkList[drinkIndex][@"reference"];
        number = [f numberFromString:numberString];
        drink.reference = number;
        drink.favorite = [NSNumber numberWithBool:NO];
        drink.info = drinkInfo;
        
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
}

-(NSError *)saveDrinkSettings:(CDBGDrinkSettings *)drinkSettings {
    CDBGDrinkSettings *newDrinkSettings = [self getDrinkSettings];
    
    newDrinkSettings.drinkType = drinkSettings.drinkType;
    newDrinkSettings.filterTypical = drinkSettings.filterTypical;
    newDrinkSettings.filterFavorites = drinkSettings.filterFavorites;
    newDrinkSettings.filterCategoriesMask = drinkSettings.filterCategoriesMask;
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    return error;
}

- (NSError *)saveDrinkType:(DrinkType)dt {
    CDBGDrinkSettings *drinkSettings = [self getDrinkSettings];
    drinkSettings.drinkType = [NSNumber numberWithInt:dt];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    return error;
}

-(NSNumber *)getDrinkCount:(DeleteSelection)type {
    int count = 0;
    for (CDBGDrink *drink in [self getDrinks])
    {
        if (type == locked && [drink.locked boolValue] == YES) {
            count++;
        }
        else if (type == unlocked && [drink.locked boolValue] == NO) {
            count++;
        }
        else if (type == both) {
            count++;
        }
    }
    return [NSNumber numberWithInt:count];
}

-(NSMutableArray *)getDrinkCategoryList {
    if (drinkType == alcoholic) {
        return [alcoholicCategoryList copy];
    }
    return [nonalcoholicCategoryList copy];
}

-(double)getAlcoholPct:(NSString *)name {
    CDBGDrinkInfo *drinkInfo = [self getDrinkInfo:name];
    
    return [drinkInfo.alcohol_pct doubleValue];
}

-(NSMutableArray *)getAlcoholPctForCategory:(int)category {
    // Use category to filter the drinks per category.
    // if category == -1, return all drinks
    // Return the Calories Per Ounce of Alcohol for alcoholic drinks.
    // Return the Calories Per Ounce for non-alcoholic drinks.
    NSMutableArray *alcoholPctList = [[NSMutableArray alloc] init];
    for (CDBGDrink *drink in [self getDrinks]) {
        CDBGDrinkInfo *drinkInfo = drink.info;
        double alcoholPct = [drinkInfo.alcohol_pct doubleValue];
        int cat = [drink.category intValue];
        if (category != -1 && cat != category) {
            continue;
        }
        [alcoholPctList addObject:[NSNumber numberWithDouble:alcoholPct]];
    }
    return alcoholPctList;
}

-(bool)isFavorite:(NSString *)drinkName {
    CDBGDrink *drink = [self getDrink:drinkName];
    return [drink.favorite boolValue];
}

-(bool)toggleFavorite:(NSString *)drinkName {
    CDBGDrink *drink = [self getDrink:drinkName];
    drink.favorite = ([drink.favorite boolValue] == YES) ? [NSNumber numberWithBool:NO] : [NSNumber numberWithBool:YES];

    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    return [drink.favorite boolValue];
}

-(NSArray *)getFavorites {
    NSMutableArray *favoriteDrinks = [[NSMutableArray alloc] init];
    for (CDBGDrink *drink in [self getDrinks]) {
        if ([drink.favorite boolValue] == YES) {
            [favoriteDrinks addObject:drink];
        }
    }
    return favoriteDrinks;
}

-(NSArray *)getReferenceDrinks {
    NSMutableArray *referenceDrinks = [[NSMutableArray alloc] init];
    for (CDBGDrink *drink in [self getDrinks]) {
        if ([drink.reference boolValue] == YES) {
            [referenceDrinks addObject:drink];
        }
    }
    return referenceDrinks;
}

-(CDBGDrinkSettings *)getDrinkSettings {
    // Try to read the drink settings from the settings in the database
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CDBGDrinkSettings" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    CDBGDrinkSettings *drinkSettings;
    
    if (fetchedObjects.count == 0) {
        // If there is no settings object saved in the database, create one
        drinkSettings = [NSEntityDescription
                            insertNewObjectForEntityForName:@"CDBGDrinkSettings"
                            inManagedObjectContext:self.managedObjectContext];
        drinkSettings.drinkType = alcoholic;
        drinkSettings.filterTypical = [NSNumber numberWithBool:YES];
        drinkSettings.filterFavorites = [NSNumber numberWithBool:YES];
        NSMutableArray *drinkCategories = [self getDrinkCategoryList];
        NSMutableString *mask = [[NSMutableString alloc] init];
        for (int i = 0; i < drinkCategories.count; i++) {
            [mask appendString:@"1 "];
        }
        drinkSettings.filterCategoriesMask = mask;
    }
    else {
        drinkSettings = fetchedObjects[0];
    }
    return drinkSettings;
}

@end
