//
//  CDBGGraphView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/26/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDBGGraphView : UIView {
    CGContextRef context;
    NSString *graphName;
    NSArray *data;
    NSDictionary *drinks;
    NSMutableArray *labelPositions;
    
    CGFloat width;
    CGFloat xLeftMargin;
    CGFloat xRightMargin;
    CGFloat minX;
    CGFloat maxX;
    CGFloat rangeOfX;
    CGFloat pctGreen;
    UIColor *lineColor;
    bool greenToRed;
    CGFloat labelLineLength;
    CGFloat height;
    CGFloat yMargin;
    CGFloat minYValue;
    CGFloat maxYValue;
    CGFloat minY;
    CGFloat maxY;
    CGFloat rangeOfYValues;
    CGFloat rangeOfY;
    NSNumber *previousValue;
    CGFloat x;
    CGFloat y;
    NSMutableArray *drinkLabels;
}

-(id)initWithFrame:(CGRect)frame graphName:(NSString *)name data:(NSArray *)dataArray drinksToDisplay:(NSDictionary *)drinkLookup;
- (void)drawRect:(CGRect)rect;
-(void)drawLineToNextPoint:(int)index;
-(void)setLineColor:(int)index;
-(void)checkForLabel:(int)index;
-(void)drawLabel:(NSString *)text;
-(CGFloat)getX:(int)index;
-(CGFloat)getY:(int)index;
-(NSArray *)tryToSplitString:(NSString *)text;
-(CGRect)adjustLabelPosition:(NSArray *)labelText x:(CGFloat)xMidpoint y:(CGFloat)yBottom;
-(bool)doesLabelIntersect:(CGRect)thisLabel;

@end
