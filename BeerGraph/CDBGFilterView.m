//
//  CDBGFilterView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 9/3/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGFilterView.h"

@implementation CDBGFilterView

-(id)initWithFrame:(CGRect)frame x:(CGFloat)x y:(CGFloat)y w:(CGFloat)w h:(CGFloat)h buttonWidth:(CGFloat)bw buttonHeight:(CGFloat)bh yPadding:(CGFloat)yp settings:(CDBGDrinkSettings *)ds categoryNames:(NSArray *)cn {
    self = [super initWithFrame:frame];
    if (self) {
        xLoc = x;
        yLoc = y;
        width = w;
        height = h;
        buttonWidth = bw;
        buttonHeight = bh;
        yPadding = yp;
        drinkSettings = ds;
        categoryNames = cn;
        categories = [[ds makeFilterCategoriesArray:ds.filterCategoriesMask] copy];

        [self setBackgroundColor:UIColorFromRGB(0x1886FB)];
        [self drawView];
    }
    return self;
}

-(void)drawView {
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE TYPICALBUTTON
    CGFloat typicalButtonX = ((width * 0.5f) - buttonWidth) * 0.5f;
    CGFloat typicalButtonY = yPadding;
    CGFloat typicalButtonW = buttonWidth;
    CGFloat typicalButtonH = buttonHeight;
    UIButton* typicalButton= [[UIButton alloc] initWithFrame:CGRectMake(typicalButtonX, typicalButtonY, typicalButtonW, typicalButtonH)];
    [typicalButton.titleLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:buttonHeight]];
    [typicalButton setTitle:@"\u2610 Typical" forState:UIControlStateNormal];    // uncheck the button in normal state
    [typicalButton setTitle:@"\u2611 Typical" forState:UIControlStateSelected];  // check the button in selected state
    [typicalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [typicalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    typicalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    typicalButton.backgroundColor = UIColorFromRGB(0x1886FB);
    typicalButton.tag = 1;
    typicalButton.selected = [drinkSettings.filterTypical boolValue];
    [typicalButton addTarget:self action:@selector(checkBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:typicalButton ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE FAVORITESBUTTON
    CGFloat favoritesButtonX = ((width * 0.5f) - buttonWidth) * 0.5f;
    CGFloat favoritesButtonY = yPadding + buttonHeight + yPadding;
    CGFloat favoritesButtonW = buttonWidth;
    CGFloat favoritesButtonH = buttonHeight;
    UIButton* favoritesButton= [[UIButton alloc] initWithFrame:CGRectMake(favoritesButtonX, favoritesButtonY, favoritesButtonW, favoritesButtonH)];
    [favoritesButton.titleLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:buttonHeight]];
    [favoritesButton setTitle:@"\u2610 Favorites" forState:UIControlStateNormal];    // uncheck the button in normal state
    [favoritesButton setTitle:@"\u2611 Favorites" forState:UIControlStateSelected];  // check the button in selected state
    [favoritesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [favoritesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    favoritesButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    favoritesButton.backgroundColor = UIColorFromRGB(0x1886FB);
    favoritesButton.tag = 2;
    favoritesButton.selected = [drinkSettings.filterFavorites boolValue];
    [favoritesButton addTarget:self action:@selector(checkBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:favoritesButton ];
    
    for (int i = 0; i < categoryNames.count; i++) {
        NSString *categoryName = categoryNames[i];
        ////////////////////////////////////////////////////////////////////////////////
        // DRAWS THE CATEGORYBUTTON
        CGFloat categoryButtonX = (width * 0.75f) - (buttonWidth * 0.5f);
        CGFloat categoryButtonY = yPadding + ((buttonHeight + yPadding) * i);
        CGFloat categoryButtonW = buttonWidth;
        CGFloat categoryButtonH = buttonHeight;
        UIButton* categoryButton= [[UIButton alloc] initWithFrame:CGRectMake(categoryButtonX, categoryButtonY, categoryButtonW, categoryButtonH)];
        [categoryButton.titleLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:buttonHeight]];
        [categoryButton setTitle:[[NSString alloc]initWithFormat:@"\u2610 %@", categoryName] forState:UIControlStateNormal];    // uncheck the button in normal state
        [categoryButton setTitle:[[NSString alloc]initWithFormat:@"\u2611 %@", categoryName] forState:UIControlStateSelected];  // check the button in selected state
        [categoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [categoryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        categoryButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        categoryButton.backgroundColor = UIColorFromRGB(0x1886FB);
        categoryButton.tag = i + 3;
        categoryButton.selected = [categories[i] boolValue];
        [categoryButton addTarget:self action:@selector(checkBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:categoryButton ];
    }
}

-(IBAction)checkBoxButtonClicked:(UIButton*)sender {
    sender.selected = !sender.selected;    // toggle button's selected state
    
    switch (sender.tag) {
        case 1: {
            drinkSettings.filterTypical = [NSNumber numberWithBool:sender.selected];
            break;
        }
        case 2: {
            drinkSettings.filterFavorites = [NSNumber numberWithBool:sender.selected];
            break;
        }
        default: {
            // Make sure that at least one category is selected
            if (sender.selected == NO) {
                int numberSelected = 0;
                for (int i = 0; i < categories.count; i++) {
                    if ([categories[i] boolValue] == YES) {
                        numberSelected++;
                    }
                }
                if (numberSelected > 1) {
                    categories[sender.tag - 3] = [NSNumber numberWithBool:sender.selected];
                }
                else {
                    sender.selected = !sender.selected;
                }
            }
            else {
                categories[sender.tag - 3] = [NSNumber numberWithBool:sender.selected];
            }
            break;
        }
    }
    [drinkSettings makeFilterCategoriesMask:categories];
    if (self.delegate && [self.delegate respondsToSelector:@selector(checkBoxButtonClicked:)]) {
        [self.delegate checkboxClickAction:drinkSettings];
    }
}

@end
