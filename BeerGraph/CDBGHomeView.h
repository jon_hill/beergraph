//
//  HomeView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/20/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGConstants.h"

@protocol HomeViewActionProtocol <NSObject>
@required
-(void)selectAction;
-(void)addAction;
-(void)settingsAction;

@end

@interface CDBGHomeView : UIView {
    UIButton *selectButton;
    UIButton *addButton;
    UIButton *settingsButton;
}

@property (nonatomic, weak) id<HomeViewActionProtocol> delegate;
@property (strong, nonatomic) CDBGDrinkManager *drinkManager;

-(id)initWithFrame:(CGRect)frame;
-(void)drawView;
-(IBAction) selectButtonClicked:(id)sender;
-(IBAction) addButtonClicked:(id)sender;
-(IBAction) settingsButtonClicked:(id)sender;

@end
