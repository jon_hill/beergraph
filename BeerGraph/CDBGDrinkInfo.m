//
//  DrinkInfo.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/7/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGDrinkInfo.h"
#import "CDBGDrink.h"


@implementation CDBGDrinkInfo

@dynamic calories;
@dynamic ounces;
@dynamic alcohol_pct;
@dynamic drink;

@end
