//
//  CDBGDisplayView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/26/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGDisplayView.h"

@implementation CDBGDisplayView

@synthesize drinkName, scroller;

-(id)   initWithFrame:(CGRect)frame
            drinkName:(NSString *)name
        drinkInfoText:(NSString *)diText
         drinkPctText:(NSString *)dpText
       caloriePctText:(NSString *)cpText
caloriePctForTypeText:(NSString *)cpftText
        graphDataList:(NSArray *)gdList; {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        drinkName = name;
        drinkInfoText = diText;
        drinkPctText = dpText;
        caloriePctText = cpText;
        caloriePctForTypeText = cpftText;
        graphDataList = gdList;
        [self drawView];
    }
    return self;
}

-(void)drawView {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat textFieldHeight = height * 0.06f;
    CGFloat textFieldWidth = width * 0.9f;
    CGFloat yLocation = 0.0f;
    CGFloat verticalBuffer = height * 0.03f;

    viewElements = [[NSMutableArray alloc] init];

    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE GRAPHSCROLLVIEW
    CGFloat graphScrollViewX = 0;
    CGFloat graphScrollViewY = yLocation;
    CGFloat graphScrollViewW = width;
    CGFloat graphScrollViewH = height - graphScrollViewY;
    graphScrollView= [[UIScrollView alloc] initWithFrame:CGRectMake(graphScrollViewX, graphScrollViewY, graphScrollViewW, graphScrollViewH)];
    graphScrollView.delegate = self;
    graphScrollView.showsVerticalScrollIndicator=YES;
    graphScrollView.showsHorizontalScrollIndicator=YES;
    graphScrollView.scrollEnabled=YES;
    graphScrollView.userInteractionEnabled=YES;
    
    yLocation = graphScrollViewH * 0.01f;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKNAMELABEL
    CGFloat drinkNameLabelW = textFieldWidth;
    CGFloat drinkNameLabelH = textFieldHeight;
    CGFloat drinkNameLabelX = (width - drinkNameLabelW) * 0.5f;
    CGFloat drinkNameLabelY = yLocation;
    UILabel* drinkNameLabel= [[UILabel alloc] initWithFrame:CGRectMake(drinkNameLabelX, drinkNameLabelY, drinkNameLabelW, drinkNameLabelH)];
    [drinkNameLabel setText:[[NSString alloc]initWithFormat:@"%@", drinkName]];
    [drinkNameLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:drinkNameLabelH ]];
    [drinkNameLabel setTextAlignment:NSTextAlignmentCenter];
    [drinkNameLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [graphScrollView addSubview:drinkNameLabel ];
    //[viewElements addObject:drinkNameLabel];
    
    yLocation += drinkNameLabelH;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKINFOLABEL
    CGFloat drinkInfoLabelW = textFieldWidth;
    CGFloat drinkInfoLabelH = textFieldHeight;
    CGFloat drinkInfoLabelX = (width - drinkInfoLabelW) * 0.5f;
    CGFloat drinkInfoLabelY = yLocation;
    UILabel* drinkInfoLabel= [[UILabel alloc] initWithFrame:CGRectMake(drinkInfoLabelX, drinkInfoLabelY, drinkInfoLabelW, drinkInfoLabelH)];
    [drinkInfoLabel setText:drinkInfoText];
    [drinkInfoLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:drinkInfoLabelH ]];
    [drinkInfoLabel setTextAlignment:NSTextAlignmentCenter];
    [drinkInfoLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [graphScrollView addSubview:drinkInfoLabel ];
    //[viewElements addObject:drinkInfoLabel];
    
    yLocation += drinkInfoLabelH + verticalBuffer;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKPCTLABEL
    CGFloat drinkPctLabelW = textFieldWidth;
    CGFloat drinkPctLabelH = textFieldHeight;
    CGFloat drinkPctLabelX = (width - drinkPctLabelW) * 0.5f;
    CGFloat drinkPctLabelY = yLocation;
    UILabel* drinkPctLabel= [[UILabel alloc] initWithFrame:CGRectMake(drinkPctLabelX, drinkPctLabelY, drinkPctLabelW, drinkPctLabelH)];
    [drinkPctLabel setText:drinkPctText];
    [drinkPctLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:drinkPctLabelH ]];
    [drinkPctLabel setTextAlignment:NSTextAlignmentCenter];
    [drinkPctLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [graphScrollView addSubview:drinkPctLabel ];
    //[viewElements addObject:drinkPctLabel];
    
    yLocation += drinkPctLabelH;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE CALORYPCTLABEL
    CGFloat caloryPctLabelW = textFieldWidth;
    CGFloat caloryPctLabelH = textFieldHeight;
    CGFloat caloryPctLabelX = (width - caloryPctLabelW) * 0.5f;
    CGFloat caloryPctLabelY = yLocation;
    UILabel* caloryPctLabel= [[UILabel alloc] initWithFrame:CGRectMake(caloryPctLabelX, caloryPctLabelY, caloryPctLabelW, caloryPctLabelH)];
    [caloryPctLabel setText:caloriePctText];
    [caloryPctLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:caloryPctLabelH ]];
    [caloryPctLabel setTextAlignment:NSTextAlignmentCenter];
    [caloryPctLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [graphScrollView addSubview:caloryPctLabel ];
    //[viewElements addObject:caloryPctLabel];
    
    yLocation += caloryPctLabelH;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE CALORYPCTFORTYPELABEL
    CGFloat caloryPctForTypeLabelW = textFieldWidth;
    CGFloat caloryPctForTypeLabelH = textFieldHeight;
    CGFloat caloryPctForTypeLabelX = (width - caloryPctForTypeLabelW) * 0.5f;
    CGFloat caloryPctForTypeLabelY = yLocation;
    UILabel* caloryPctForTypeLabel= [[UILabel alloc] initWithFrame:CGRectMake(caloryPctForTypeLabelX, caloryPctForTypeLabelY, caloryPctForTypeLabelW, caloryPctForTypeLabelH)];
    [caloryPctForTypeLabel setText:caloriePctForTypeText];
    [caloryPctForTypeLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:caloryPctForTypeLabelH ]];
    [caloryPctForTypeLabel setTextAlignment:NSTextAlignmentCenter];
    [caloryPctForTypeLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [graphScrollView addSubview:caloryPctForTypeLabel ];
    //[viewElements addObject:caloryPctForTypeLabel];

    yLocation += caloryPctForTypeLabelH;
    
    // graphDataList contains a list of graphs to draw.  Set the
    // graphScrollView.contentSize large enough to contain all the graphs.
    graphHeight = graphScrollViewH * 0.5f;
    CGFloat graphContentSize = yLocation + (graphHeight * (double)graphDataList.count);
    graphScrollView.contentSize = CGSizeMake(graphScrollViewW, graphContentSize);

    graphYLocation = yLocation;
    graphWidth = graphScrollViewW;
    
    [self drawGraphs];
    [self addSubview:graphScrollView ];
}

-(void)drawGraphs {
    CGFloat yLocation = graphYLocation;
    
    for (CDBGGraphData *graphData in graphDataList) {
        ////////////////////////////////////////////////////////////////////////////////
        // DRAWS EACH GRAPH VIEW
        CGFloat graphViewX = 0;
        CGFloat graphViewY = yLocation;
        CGFloat graphViewW = graphWidth;
        CGFloat graphViewH = graphHeight;
        CDBGGraphView* graphView = [[CDBGGraphView alloc] initWithFrame:CGRectMake(graphViewX, graphViewY, graphViewW, graphViewH) graphName:graphData.graphName data:graphData.data drinksToDisplay:graphData.displayData];
        [graphScrollView addSubview:graphView];
        [viewElements addObject:graphView];
        
        yLocation += graphViewH;
    }
    
}

-(void)updateView:(NSArray *)gdList {
    for (UIView *viewElement in viewElements) {
        [viewElement removeFromSuperview];
    }
    [viewElements removeAllObjects];
    [self drawGraphs];
}

@end
