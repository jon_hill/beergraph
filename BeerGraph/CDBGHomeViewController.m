//
//  HomeViewController.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/12/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGHomeViewController.h"
#import "CDBGSelectViewController.h"
#import "CDBGAddViewController.h"
#import "CDBGSettingsViewController.h"

@interface CDBGHomeViewController ()

@end

@implementation CDBGHomeViewController

@synthesize managedObjectContext, drinkManager;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = @"Home";

    drinkManager = [[CDBGDrinkManager alloc] init:managedObjectContext];
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;

    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE HOMEVIEW
    CGFloat homeViewX = 0;
    CGFloat homeViewY = 0;
    CGFloat homeViewW = width;
    CGFloat homeViewH = height;
    homeView= [[CDBGHomeView alloc] initWithFrame:CGRectMake(homeViewX, homeViewY, homeViewW, homeViewH)];
    homeView.delegate = self;

    [self.view addSubview:homeView ];
    
    // Test listing all drinks from the data store
    NSLog(@"Selected Drink Type: %d", drinkManager.drinkType);
    NSArray *fetchedObjects = [drinkManager getDrinks];
    for (CDBGDrink *drink in fetchedObjects) {
        CDBGDrinkInfo *drinkInfo = drink.info;
        NSLog(@"Name: %@", drink.name);
        NSLog(@"Calories: %@", drinkInfo.calories);
        NSLog(@"Ounces: %@", drinkInfo.ounces);
        NSLog(@"Alcohol_pct: %@", drinkInfo.alcohol_pct);
    }

}

#pragma mark - HomeViewActionProtocol

- (void)selectAction {
    CDBGSelectViewController *selectViewController = [[CDBGSelectViewController alloc] init];
    selectViewController.drinkManager = self.drinkManager;
    [self.navigationController pushViewController:selectViewController animated:YES];
}

- (void)addAction {
    CDBGAddViewController *addViewController = [[CDBGAddViewController alloc] init];
    addViewController.drinkManager = self.drinkManager;
    [self.navigationController pushViewController:addViewController animated:YES];
}

- (void)settingsAction {
    CDBGSettingsViewController *settingsViewController = [[CDBGSettingsViewController alloc] init];
    settingsViewController.drinkManager = self.drinkManager;
    [self.navigationController pushViewController:settingsViewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
