//
//  DrinkManager.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/13/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDBGDrink.h"
#import "CDBGDrinkInfo.h"
#import "CDBGDrinkSettings.h"

@interface CDBGDrinkManager : NSObject

typedef NS_ENUM(NSUInteger, DrinkType) {
    alcoholic,
    nonAlcoholic,
    allDrinks
};

typedef NS_ENUM(NSUInteger, DeleteSelection) {
    locked,
    unlocked,
    both
};

@property (nonatomic) DrinkType drinkType;
@property (nonatomic) NSNumber *filterTypical;
@property (nonatomic) NSNumber *filterFavorites;
@property (nonatomic) NSArray *filterCategories;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSArray *alcoholicCategoryList;
@property (nonatomic, strong) NSArray *nonalcoholicCategoryList;

-(id)init:(NSManagedObjectContext *)moc;
-(NSArray *)getDrinks;
-(NSArray *)getDrinkNames;
-(BOOL)doesDrinkExist:(NSString *)name;
-(NSString *)getDrinkCategory:(NSString *)name;
-(NSNumber *)getCategoryNumber:(NSString *)category;
-(NSError *)saveDrink:(NSString *)drinkName cal:(NSNumber *)calories oz:(NSNumber *)ounces alc:(NSNumber *)alcohol_pct cat:(NSNumber *)category fav:(NSNumber *)favorite ref:(NSNumber *)reference;
-(NSError *)deleteDrink:(NSString *)name;
-(NSError *)deleteDrinks:(DeleteSelection)selection;
-(CDBGDrink *)getDrink:(NSString *)name;
-(CDBGDrinkInfo *)getDrinkInfo:(NSString *)name;
-(double)getCaloriesPerOunce:(NSString *)name;
-(NSMutableArray *)getAllDrinkCaloriesPerOunceForCategory:(int)category;
-(void)importDrinkList;
-(NSError *)saveDrinkSettings:(CDBGDrinkSettings *)drinkSettings;
-(NSError *)saveDrinkType:(DrinkType)dt;
-(NSNumber *)getDrinkCount:(DeleteSelection)type;
-(NSMutableArray *)getDrinkCategoryList;
-(double)getAlcoholPct:(NSString *)drinkName;
-(NSMutableArray *)getAlcoholPctForCategory:(int)category;
-(bool)isFavorite:(NSString *)drinkName;
-(bool)toggleFavorite:(NSString *)drinkName;
-(NSArray *)getFavorites;
-(NSArray *)getReferenceDrinks;
-(CDBGDrinkSettings *)getDrinkSettings;
+(NSArray *)getDrinkCategories;

@end
