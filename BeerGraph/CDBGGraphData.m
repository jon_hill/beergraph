//
//  CDBGGraphData.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/27/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGGraphData.h"

@implementation CDBGGraphData

@synthesize graphName, data, displayData;

-(id)init:name data:(NSArray *)graphData displayData:(NSDictionary *)graphDisplayData {
    self = [super init];
    if (self) {
        graphName = name;
        data = graphData;
        displayData = graphDisplayData;
    }
    return self;
}

@end
