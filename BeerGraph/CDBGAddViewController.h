//
//  CDBGAddViewController.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/25/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGAddView.h"
#import "CDBGDisplayViewController.h"

@interface CDBGAddViewController : UIViewController <AddViewActionProtocol, UIActionSheetDelegate> {
    CDBGAddView *addView;
    NSString *newDrinkName;
    NSNumber *drinkType, *calories, *ounces, *alcoholPct;
}

@property (strong, nonatomic) CDBGDrinkManager *drinkManager;

-(void)viewDidLoad;
-(void)viewWillAppear:(BOOL)animated;
-(void)viewWillDisappear:(BOOL)animated;
-(void)createAction:(NSString *)drinkName drinkType:(NSNumber *)type calories:(NSNumber *)cal ounces:(NSNumber *)oz alcoholPct:(NSNumber *)alcPct;
-(void)cancelAction;
-(void)overwriteDrink:(NSString *)name;
-(void)cancelDrink:(NSString *)name;
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;

@end
