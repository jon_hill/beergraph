//
//  CDBGDisplayView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/26/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGConstants.h"
#import "CDBGDrinkManager.h"
#import "CDBGGraphView.h"
#import "CDBGGraphData.h"

@interface CDBGDisplayView : UIView <UIScrollViewDelegate> {
    NSString *drinkInfoText;
    NSString *drinkPctText;
    NSString *caloriePctText;
    NSString *caloriePctForTypeText;
    NSArray *graphDataList;
    NSMutableArray *viewElements;
    CGFloat graphHeight;
    CGFloat graphWidth;
    CGFloat graphYLocation;
    UIScrollView *scroller;
    UIScrollView *graphScrollView;
}

@property (nonatomic, strong) NSString *drinkName;
@property (nonatomic, retain) IBOutlet UIScrollView *scroller;

-(id)   initWithFrame:(CGRect)frame
            drinkName:(NSString *)name
        drinkInfoText:(NSString *)diText
         drinkPctText:(NSString *)dpText
       caloriePctText:(NSString *)cpText
caloriePctForTypeText:(NSString *)cpftText
        graphDataList:(NSArray *)gdList;
-(void)drawView;
-(void)drawGraphs;
-(void)updateView:(NSArray *)gdList;

@end
