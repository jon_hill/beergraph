//
//  CDBGDisplayViewController.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/24/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGDisplayView.h"
#import "CDBGGraphData.h"
#import "CDBGConstants.h"
#import "CDBGFilterView.h"

@interface CDBGDisplayViewController : UIViewController <FilterViewActionProtocol> {
    CDBGDisplayView *displayView;
    CDBGFilterView *settingsView;
    bool settingsViewActive;
    UIButton *settingsButton;
    UIBarButtonItem *settingsNavigationButton;
    CGFloat displayViewW;
    CGFloat displayViewH;
    CGFloat settingsViewHeight;
    CDBGDrinkInfo *drinkInfo;
    CDBGDrinkSettings *drinkSettings;
}


@property (nonatomic, strong) CDBGDrinkManager *drinkManager;
@property (nonatomic, strong) NSString *drinkName;
@property (nonatomic, strong) NSMutableArray *graphDataList;

-(void)viewDidLoad;
-(void)drawDisplayView;
-(IBAction)settingsButtonClicked:(id)sender;
-(NSString *)getDrinkInfoLabelText;
-(NSString *)getCaloriesPerOunceLabelText;
-(NSString *)getCaloriePctOfAllDrinks;
-(NSString *)getCaloriePctOfThisDrinkType;
-(CDBGGraphData *)getCaloriesPerOunceGraphData;
-(NSArray *)getSortedCaloriesList;
-(CDBGGraphData *)getAlcoholPercentByVolumeGraphData;
-(NSArray *)getSortedAlcoholList;
-(void)addToDictionary:(NSMutableDictionary *)displayList drink:(NSString *)drinkName index:(double)index;
-(void)createGraphData;

@end
