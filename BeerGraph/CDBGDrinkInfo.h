//
//  DrinkInfo.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/7/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDBGDrink;

@interface CDBGDrinkInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * calories;
@property (nonatomic, retain) NSNumber * ounces;
@property (nonatomic, retain) NSNumber * alcohol_pct;
@property (nonatomic, retain) CDBGDrink *drink;

@end
