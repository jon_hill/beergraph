//
//  CDBGGraphView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/26/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGGraphView.h"

@implementation CDBGGraphView

-(id)initWithFrame:(CGRect)frame graphName:(NSString *)name data:(NSArray *)dataArray drinksToDisplay:(NSDictionary *)drinkLookup {
    self = [super initWithFrame:frame];
    if (self) {
        graphName = name;
        data = dataArray;
        drinks = drinkLookup;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    context = UIGraphicsGetCurrentContext();

    CGContextSetLineWidth(context, 0.6);

    width = self.frame.size.width;
    xLeftMargin = 0.1f;
    xRightMargin = 0.05f;
    minX = width * xLeftMargin;
    maxX = width - (width * xRightMargin);
    rangeOfX = maxX - minX;
    
    height = self.frame.size.height;
    yMargin = 0.1f;
    minYValue = [data[0] doubleValue];
    maxYValue = [data[data.count - 1] doubleValue];
    // If the data is sorted highest to lowest, get the correct minYValue
    // and maxYValue, and set greenToRed = NO, indicating that the graph
    // color should go from red to green and not green to red
    if (minYValue > maxYValue) {
        minYValue = [data[data.count - 1] doubleValue];
        maxYValue = [data[0] doubleValue];
        greenToRed = NO;
    }
    else {
        greenToRed = YES;
    }
    minY = height * yMargin;
    maxY = height - (height * yMargin);
    rangeOfYValues = maxYValue - minYValue;
    rangeOfY = maxY - minY;
    labelLineLength = rangeOfY * 0.075f;
    
    labelPositions = [[NSMutableArray alloc] init];
    
    drinkLabels = [[NSMutableArray alloc] init];
    
    // Draw the X and Y axis
    CGContextSetStrokeColorWithColor(context, [[UIColor whiteColor] CGColor]);
    CGContextMoveToPoint(context, minX, minY);
    CGContextAddLineToPoint(context, minX, maxY);
    CGContextAddLineToPoint(context, maxX, maxY);
    CGContextStrokePath(context);

    // previousValue is used to make sure that multiple labels are not drawn
    // if there are several drinks with the same value in the graph
    previousValue = [NSNumber numberWithDouble:-1.0f];

    // Move to the location of the first data point
    x = [self getX:0];
    y = [self getY:0];
    //NSLog(@"X:%2.1f  Y:%2.1f  CAL: %@", x, y, data[0]);
    CGContextMoveToPoint(context, x, y);
    
    // Check to see if this point needs a label
    [self setLineColor:0];
    [self checkForLabel:0];

    for (int i = 1; i < data.count; i++) {
        // For each drink, draw a colored line to the next point and check
        // to see if this point needs a label
        x = [self getX:i];
        y = [self getY:i];
        //NSLog(@"X:%2.1f  Y:%2.1f  CAL: %@", x, y, data[i]);

        [self drawLineToNextPoint:i];
        [self checkForLabel:i];
    }
    
    // Draw the Y axis labels
    CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
    CGContextSetFillColorWithColor(context, [[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] CGColor]);
    CGContextSelectFont(context, "Helvetica", 8, kCGEncodingMacRoman);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    NSString *theText;
    for (CGFloat yLabel = minYValue; yLabel <= maxYValue; yLabel += (rangeOfYValues * 0.2f)) {
        theText = [NSString stringWithFormat:@"%d", (int)yLabel];
        CGSize labelSize = [theText sizeWithFont:[UIFont fontWithName:@"Helvetica" size:8]];
        y = maxY - (((yLabel - minYValue) / rangeOfYValues) * rangeOfY);
        //NSLog(@"X:%2.1f  Y:%2.1f  TEXT: %@", 0.0f, y, theText);
        y += (labelSize.height / 2);
        CGContextShowTextAtPoint(context, minX * 0.5f, y, [theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);
    }
    
    // Draw the graph label
    CGContextSelectFont(context, "Helvetica", 12, kCGEncodingMacRoman);
    theText = [NSString stringWithFormat:@"%@", graphName];
    CGSize labelSize = [theText sizeWithFont:[UIFont fontWithName:@"Helvetica" size:8]];
    CGContextShowTextAtPoint(context, (width - labelSize.width) * 0.5f, labelSize.height * 1.25f, [theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);

    CGContextStrokePath(context);
}

-(void)drawLineToNextPoint:(int)index {
    [self setLineColor:index];
    CGContextAddLineToPoint(context, x, y);
    CGContextStrokePath(context);
    CGContextMoveToPoint(context, x, y);
}

-(void)setLineColor:(int)index {
    pctGreen = ([data[index] doubleValue] - minYValue) / rangeOfYValues;
    if (greenToRed == YES) {
        pctGreen = 1.0 - pctGreen;
    }
    //NSLog(@"pctGreen: %2.3f", pctGreen);
    lineColor = [UIColor colorWithHue:(pctGreen / 3.0f) saturation:1.0 brightness:1.0 alpha:1.0];
    CGContextSetStrokeColorWithColor(context, [lineColor CGColor]);
    CGContextSetFillColorWithColor(context, [lineColor CGColor]);
}

-(void)checkForLabel:(int)index {
    if (drinkLabels.count > 0) {
        NSString *drinkName = drinkLabels[0];
        [drinkLabels removeObjectAtIndex:0];
        [self drawLabel:drinkName];
    }
    else if ([drinks objectForKey:data[index]] != nil && [previousValue doubleValue] != [data[index] doubleValue]) {
        drinkLabels = [drinks objectForKey:data[index]];
        NSString *drinkName = drinkLabels[0];
        [drinkLabels removeObjectAtIndex:0];
        [self drawLabel:drinkName];
    }
    previousValue = data[index];
}

-(void)drawLabel:(NSString *)text {
    int circleRadius = 3;

    CGRect rect = CGRectMake(x - circleRadius, y - circleRadius, 2 * circleRadius, 2 * circleRadius);
    CGContextAddEllipseInRect(context, rect);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    NSArray *splitString = [NSArray arrayWithObjects:text, nil];
    if ([text length] > 12) {
        splitString = [self tryToSplitString:text];
    }
    CGFloat newY = (y > (minY + (rangeOfY * 0.5f)) ? y - labelLineLength : y + labelLineLength);
    CGRect textLocation = [self adjustLabelPosition:splitString x:x y:newY];
    [labelPositions addObject:[NSValue valueWithCGRect:textLocation]];
    CGFloat labelY = textLocation.origin.y + textLocation.size.height;
    CGFloat newX = textLocation.origin.x;

    // Draw the vertical line to the label
    CGContextMoveToPoint(context, x, y);
    if (labelY > y) {
        CGContextAddLineToPoint(context, x, labelY - textLocation.size.height);
    }
    else {
        CGContextAddLineToPoint(context, x, labelY);
    }
    CGContextStrokePath(context);

    // Draw the text label
    for (int i = splitString.count - 1; i >= 0; i--) {
        CGSize labelSize = [splitString[i] sizeWithFont:[UIFont fontWithName:@"Helvetica" size:8]];
        newX = x - (labelSize.width * 0.5f);
        if (newX < minX) {
            newX = minX;
        }
        if (newX + labelSize.width > maxX) {
            newX = maxX - labelSize.width;
        }
        CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
        CGContextSetStrokeColorWithColor(context, [lineColor CGColor]);
        CGContextSetFillColorWithColor(context, [lineColor CGColor]);
        CGContextSelectFont(context, "Helvetica", 8, kCGEncodingMacRoman);
        CGContextSetTextDrawingMode(context, kCGTextFill);
        CGContextShowTextAtPoint(context, newX, labelY, [splitString[i] cStringUsingEncoding:NSUTF8StringEncoding], [splitString[i] length]);
        CGContextStrokePath(context);
        labelY -= labelSize.height * 0.8f;
    }
    CGContextMoveToPoint(context, x, y);
}

-(CGFloat)getX:(int)index {
    return (((double)(index + 1) / (double)data.count) * (double)rangeOfX) + (double)minX;
}

-(CGFloat) getY:(int)index {
    return maxY - ((([data[index] doubleValue] - (double)minYValue) / (double)rangeOfYValues) * (double)rangeOfY);
}

-(NSArray *)tryToSplitString:(NSMutableString *)text {
    NSArray *shortestSplit = [NSArray arrayWithObjects:text, nil];
    int shortestLength = [text length];
    NSArray *words = [text componentsSeparatedByString:@" "];
    for (int i = 0; i < words.count - 1; i++) {
        NSMutableString *firstString = [NSMutableString stringWithFormat:@"%@", words[0]];
        for (int j = 1; j <= i; j++) {
            [firstString appendFormat:@" %@", words[j]];
        }
        NSMutableString *secondString = [NSMutableString stringWithFormat:@"%@", words[i+1]];
        for (int k = i+2; k < words.count; k++) {
            [secondString appendFormat:@" %@", words[k]];
        }
        int longestSubstring = MAX([firstString length], [secondString length]);
        if (longestSubstring < shortestLength) {
            shortestLength = longestSubstring;
            shortestSplit = [NSArray arrayWithObjects:firstString, secondString, nil];
        }
    }
    return shortestSplit;
}

-(CGRect) adjustLabelPosition:(NSArray *)labelText x:(CGFloat)xMidpoint y:(CGFloat)yBottom {
    typedef NS_ENUM(NSUInteger, GraphDirection) {
        down,
        up
    };
    
    bool checkUp = YES;
    bool checkDown = YES;
    int direction = (yBottom > (minY + (rangeOfY * 0.5f)) ? up : down);
    CGSize labelSize;
    
    CGFloat labelX = maxX, labelY = yBottom, labelWidth = 0, labelHeight = 0, labelRowHeight = 0;
    // Get the coordinates and size of the label rectangle
    for (int i = 0; i < labelText.count; i++) {
        labelSize = [labelText[i] sizeWithFont:[UIFont fontWithName:@"Helvetica" size:8]];
        if (xMidpoint - (labelSize.width * 0.5) < labelX) {
            labelX = xMidpoint - (labelSize.width * 0.5);
            labelWidth = labelSize.width;
        }
        labelHeight += labelSize.height;
        labelRowHeight = labelSize.height;
    }
    // Check the X boundaries
    if (labelX < minX) {
        labelX = minX;
    }
    if (labelX + labelWidth > maxX) {
        labelX = maxX - labelWidth;
    }

    // Make a rectangle for the label text
    CGRect labelRect = CGRectMake(labelX, labelY - labelHeight, labelWidth, labelHeight);
    
    CGFloat adjustedY = labelY;
    while (checkUp == YES && checkDown == YES) {
        // If the current label position does not intersect any of the other
        // labels, return it
        if ([self doesLabelIntersect:labelRect] == NO) {
            return labelRect;
        }
        // If this label intersects another label, try to move this label
        adjustedY = (direction == down) ? adjustedY + labelRowHeight : adjustedY - labelRowHeight;
        if ((adjustedY + labelHeight) > maxY) {
            // If we have adjusted the label higher than the top edge of the
            // graph, stop moving up
            checkUp = NO;
            if (checkDown == YES) {
                // If we have not checked down yet, reset the label position
                // and start checking down
                direction = up;
                adjustedY = labelY - labelRowHeight;
            }
        }
        if ((adjustedY) < minY) {
            // If we have adjusted the label lower than the bottom edge of
            // the graph, stop moving down
            checkDown = NO;
            if (checkUp == YES) {
                // If we have not checked up yet, reset the label position
                // and start checking up
                direction = down;
                adjustedY = labelY + labelRowHeight;
            }
        }
        labelRect = CGRectMake(labelX, adjustedY - labelHeight, labelWidth, labelHeight);
    }
    return CGRectMake(labelX, labelY - labelHeight, labelWidth, labelHeight);
}

-(bool)doesLabelIntersect:(CGRect)thisLabel {
    for (int i = 0; i < labelPositions.count; i++) {
        CGRect label = [[labelPositions objectAtIndex:i] CGRectValue];
        if (CGRectIntersectsRect(thisLabel, label)) {
            return YES;
        }
    }
    return NO;
}

@end
