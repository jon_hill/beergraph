//
//  DrinkSettings.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/19/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDBGDrinkSettings : NSManagedObject

@property (nonatomic, retain) NSNumber * drinkType;
@property (nonatomic, retain) NSNumber * filterTypical;
@property (nonatomic, retain) NSNumber * filterFavorites;
@property (nonatomic, retain) NSString * filterCategoriesMask;

-(NSString *)makeFilterCategoriesMask:(NSArray *)fc;
-(NSArray *)makeFilterCategoriesArray:(NSString *)fc;

@end
