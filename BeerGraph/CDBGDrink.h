//
//  Drink.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/20/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDBGDrinkInfo;

@interface CDBGDrink : NSManagedObject

@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSNumber * reference;
@property (nonatomic, retain) NSNumber * category;
@property (nonatomic, retain) NSNumber * locked;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) CDBGDrinkInfo *info;

@end
