//
//  CDBGFilterView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 9/3/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGConstants.h"
#import "CDBGDrinkSettings.h"

@protocol FilterViewActionProtocol <NSObject>
@required
-(void)checkboxClickAction:(CDBGDrinkSettings *)drinkSettings;

@end

@interface CDBGFilterView : UIView {
    CGFloat xLoc, yLoc, width, height, buttonWidth, buttonHeight, yPadding;
    NSArray *categoryNames;
    NSMutableArray *categories;
    CDBGDrinkSettings *drinkSettings;
}

@property (nonatomic, weak) id<FilterViewActionProtocol> delegate;

-(id)initWithFrame:(CGRect)frame x:(CGFloat)x y:(CGFloat)y w:(CGFloat)w h:(CGFloat)h buttonWidth:(CGFloat)bw buttonHeight:(CGFloat)bh yPadding:(CGFloat)yp settings:(CDBGDrinkSettings *)ds categoryNames:(NSArray *)cn;
-(IBAction)checkBoxButtonClicked:(UIButton*)sender;

@end
