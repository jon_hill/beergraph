//
//  CDBGSettingsViewController.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/24/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGSettingsViewController.h"

@interface CDBGSettingsViewController ()

@end

@implementation CDBGSettingsViewController

@synthesize drinkManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = @"Settings";
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SETTINGSVIEW
    CGFloat settingsViewX = 0;
    CGFloat settingsViewY = 0;
    CGFloat settingsViewW = width;
    CGFloat settingsViewH = height;
    settingsView= [[CDBGSettingsView alloc] initWithFrame:CGRectMake(settingsViewX, settingsViewY, settingsViewW, settingsViewH) drinkManager:drinkManager];
    settingsView.delegate = self;
    [self.view addSubview:settingsView ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)drinkTypeSelectdAction:(NSNumber *)selection {
    switch ([selection intValue]) {
        case 0:{
            drinkManager.drinkType = alcoholic;
            break;
        }
        case 1:{
            drinkManager.drinkType = nonAlcoholic;
            break;
        }
    }
    [drinkManager saveDrinkType:drinkManager.drinkType];
}

-(void)importDrinksAction {
    [self confirmDialog:@"This will update the list of predefined drinks.  Any drinks you have created with the same name will be overwritten." tag:1];
}

-(void)deleteUserCreatedDrinksAction {
    [self confirmDialog:@"This will delete all drinks that you have created." tag:2];
}

-(void)deleteAllDrinksAction {
    [self confirmDialog:@"This will delete all drinks." tag:3];
}

- (void)confirmDialog:(NSString *)text tag:(int)tag {
    UIActionSheet *confirmActionSheet = [[UIActionSheet alloc] initWithTitle: [[NSString alloc] initWithFormat:@"%@", text] delegate:self                                                    cancelButtonTitle:nil destructiveButtonTitle:nil                                                otherButtonTitles:@"OK", @"Cancel", nil];
    [confirmActionSheet setTag:tag];
    confirmActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [confirmActionSheet showInView:self.view];
}

- (void)drinkStatusDialog:(NSString *)text tag:(int)tag {
    UIActionSheet *drinkStatusActionSheet = [[UIActionSheet alloc] initWithTitle: [[NSString alloc] initWithFormat:@"%@", text] delegate:self                                                    cancelButtonTitle:nil destructiveButtonTitle:nil                                                otherButtonTitles:@"OK", nil];
    [drinkStatusActionSheet setTag:tag];
    drinkStatusActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [drinkStatusActionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        switch (actionSheet.tag) {
            case 1: {  // importDrinks
                [drinkManager importDrinkList];
                NSString *text = [[NSString alloc]initWithFormat:@"%@ drinks imported", [drinkManager getDrinkCount:locked]];
                [self drinkStatusDialog:text tag:4];
                break;
            }
            case 2: {  // deleteUserDefinedDrinks
                NSNumber *preCount = [drinkManager getDrinkCount:unlocked];
                [drinkManager deleteDrinks:unlocked];
                NSString *text = [[NSString alloc]initWithFormat:@"%@ drinks deleted", preCount];
                [self drinkStatusDialog:text tag:4];
                break;
            }
            case 3: {  // deleteAllDrinks
                NSNumber *preCount = [drinkManager getDrinkCount:both];
                [drinkManager deleteDrinks:both];
                NSString *text = [[NSString alloc]initWithFormat:@"%@ drinks deleted", preCount];
                [self drinkStatusDialog:text tag:4];
                break;
            }
        }
    }
}

@end
