//
//  Drink.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/20/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGDrink.h"
#import "CDBGDrinkInfo.h"


@implementation CDBGDrink

@dynamic favorite;
@dynamic reference;
@dynamic category;
@dynamic locked;
@dynamic name;
@dynamic info;

@end
