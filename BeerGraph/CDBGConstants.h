//
//  Constants.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/21/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#ifndef BeerGraph_CDBGConstants_h
#define BeerGraph_CDBGConstants_h

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#endif
