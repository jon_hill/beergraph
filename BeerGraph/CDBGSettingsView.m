//
//  CDBGSettingsView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/24/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGSettingsView.h"

@implementation CDBGSettingsView

@synthesize drinkManager;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        [self drawView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame drinkManager:(CDBGDrinkManager *)dm {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        drinkManager = dm;
        [self drawView];
    }
    return self;
}

-(void)drawView {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat buttonHeight = height * 0.06f;
    CGFloat buttonWidth = width * 0.72f;
    CGFloat yLocation = height * 0.08f;
    CGFloat verticalBuffer = height * 0.08f;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKTYPELABEL
    CGFloat drinkTypeLabelW = width * 0.5f;
    CGFloat drinkTypeLabelH = height * 0.1f;
    CGFloat drinkTypeLabelX = (width - drinkTypeLabelW) * 0.5f;
    CGFloat drinkTypeLabelY = yLocation;
    UILabel* drinkTypeLabel= [[UILabel alloc] initWithFrame:CGRectMake(drinkTypeLabelX, drinkTypeLabelY, drinkTypeLabelW, drinkTypeLabelH)];
    [drinkTypeLabel setText:@"Drink Type"];
    [drinkTypeLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:drinkTypeLabelH ]];
    [drinkTypeLabel setTextAlignment:NSTextAlignmentCenter];
    [drinkTypeLabel setTextColor:UIColorFromRGB(0x1886FB)];
    [self addSubview:drinkTypeLabel ];

    yLocation += drinkTypeLabelH;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DRINKTYPESEGMENTEDCONTROL
    CGFloat drinkTypeSegmentedControlW = buttonWidth;
    CGFloat drinkTypeSegmentedControlH = buttonHeight;
    CGFloat drinkTypeSegmentedControlX = (width - drinkTypeSegmentedControlW) * 0.5f;
    CGFloat drinkTypeSegmentedControlY = yLocation;
    drinkTypeSegmentedControl= [[UISegmentedControl alloc]initWithItems:@[@"Alcoholic",@"Non-Alcoholic"]];
    drinkTypeSegmentedControl.frame = CGRectMake(drinkTypeSegmentedControlX, drinkTypeSegmentedControlY, drinkTypeSegmentedControlW, drinkTypeSegmentedControlH);
    drinkTypeSegmentedControl.selectedSegmentIndex = (drinkManager.drinkType == alcoholic ? 0 : 1);
    [drinkTypeSegmentedControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:drinkTypeSegmentedControl ];
    
    yLocation += drinkTypeSegmentedControlH + (verticalBuffer * 2.0f);
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE IMPORTDRINKSBUTTON
    CGFloat importDrinksButtonW = buttonWidth;
    CGFloat importDrinksButtonH = buttonHeight;
    CGFloat importDrinksButtonX = (width - importDrinksButtonW) * 0.5f;
    CGFloat importDrinksButtonY = yLocation;
    importDrinksButton= [[UIButton alloc] initWithFrame:CGRectMake(importDrinksButtonX, importDrinksButtonY, importDrinksButtonW, importDrinksButtonH)];
    [importDrinksButton setTitle:@"Import Drinks" forState:UIControlStateNormal];
    [importDrinksButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [importDrinksButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    importDrinksButton.layer.cornerRadius = buttonHeight/8.0f;
    [importDrinksButton addTarget:self action:@selector(importDrinksButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:importDrinksButton ];
    
    yLocation += importDrinksButtonH + verticalBuffer;

    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DELETEUSERCREATEDDRINKSBUTTON
    CGFloat deleteUserCreatedDrinksButtonW = buttonWidth;
    CGFloat deleteUserCreatedDrinksButtonH = buttonHeight;
    CGFloat deleteUserCreatedDrinksButtonX = (width - deleteUserCreatedDrinksButtonW) * 0.5f;
    CGFloat deleteUserCreatedDrinksButtonY = yLocation;
    deleteUserCreatedDrinksButton= [[UIButton alloc] initWithFrame:CGRectMake(deleteUserCreatedDrinksButtonX, deleteUserCreatedDrinksButtonY, deleteUserCreatedDrinksButtonW, deleteUserCreatedDrinksButtonH)];
    [deleteUserCreatedDrinksButton setTitle:@"Delete User Created Drinks" forState:UIControlStateNormal];
    [deleteUserCreatedDrinksButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deleteUserCreatedDrinksButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    deleteUserCreatedDrinksButton.layer.cornerRadius = buttonHeight/8.0f;
    [deleteUserCreatedDrinksButton addTarget:self action:@selector(deleteUserDrinksClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteUserCreatedDrinksButton ];

    yLocation += deleteUserCreatedDrinksButtonH + verticalBuffer;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE DELETEALLDRINKSBUTTON
    CGFloat deleteAllDrinksButtonW = buttonWidth;
    CGFloat deleteAllDrinksButtonH = buttonHeight;
    CGFloat deleteAllDrinksButtonX = (width - deleteAllDrinksButtonW) * 0.5f;
    CGFloat deleteAllDrinksButtonY = yLocation;
    deleteAllDrinksButton= [[UIButton alloc] initWithFrame:CGRectMake(deleteAllDrinksButtonX, deleteAllDrinksButtonY, deleteAllDrinksButtonW, deleteAllDrinksButtonH)];
    [deleteAllDrinksButton setTitle:@"Delete All Drinks" forState:UIControlStateNormal];
    [deleteAllDrinksButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deleteAllDrinksButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    deleteAllDrinksButton.layer.cornerRadius = buttonHeight/8.0f;
    [deleteAllDrinksButton addTarget:self action:@selector(deleteAllDrinksClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteAllDrinksButton ];
    
    yLocation += deleteAllDrinksButtonH + verticalBuffer;
}

#pragma mark - UISegmentedControl actions

-(IBAction)segmentedControlValueDidChange:(UISegmentedControl *)segment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(drinkTypeSelectdAction:)]) {
        NSNumber *selection = [[NSNumber alloc] initWithInteger:segment.selectedSegmentIndex];
        [self.delegate drinkTypeSelectdAction:selection];
    }
}

#pragma mark - UIButton actions

-(IBAction)importDrinksButtonClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(importDrinksAction)]) {
        [self.delegate importDrinksAction];
    }
}

-(IBAction)deleteUserDrinksClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteUserCreatedDrinksAction)]) {
        [self.delegate deleteUserCreatedDrinksAction];
    }
}

-(IBAction)deleteAllDrinksClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteAllDrinksAction)]) {
        [self.delegate deleteAllDrinksAction];
    }
}

@end
