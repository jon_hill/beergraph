//
//  HomeView.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/20/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGHomeView.h"
#import "CDBGHomeViewController.h"

@implementation CDBGHomeView

@synthesize drinkManager;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        [self drawView];
    }
    return self;
}

-(void)drawView {
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat buttonHeight = height * 0.06f;
    CGFloat yLocation = height * 0.1f;
    CGFloat yPadding = height * 0.04f;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE BACKGROUNDIMAGE
    CGFloat backgroundImageX = 0;
    CGFloat backgroundImageY = 0;
    CGFloat backgroundImageW = width;
    CGFloat backgroundImageH = height;
    UIImageView* backgroundImage= [[UIImageView alloc] initWithFrame:CGRectMake(backgroundImageX, backgroundImageY, backgroundImageW, backgroundImageH)];
    [backgroundImage setImage:[UIImage imageNamed:@"HomeViewBackground.jpeg"]];
    [self addSubview:backgroundImage ];
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE TITLELABEL
    CGFloat titleLabelW = width * .5f;
    CGFloat titleLabelH = buttonHeight;
    CGFloat titleLabelX = (width - titleLabelW) * .5f;
    CGFloat titleLabelY = yLocation;
    UILabel* titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(titleLabelX, titleLabelY, titleLabelW, titleLabelH)];
    [titleLabel setText:@"Drink-lytics"];
    [titleLabel setFont:[UIFont fontWithName:@"GothamHTF-Book" size:titleLabelH * 0.9f]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setBackgroundColor:UIColorFromRGB(0x1886FB)];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [self addSubview:titleLabel ];
    
    yLocation += titleLabelH + (yPadding * 2.0f);
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SELECTBUTTON
    CGFloat selectButtonW = width * .4f;
    CGFloat selectButtonH = buttonHeight;
    CGFloat selectButtonX = (width - selectButtonW) * .5f;
    CGFloat selectButtonY = yLocation;
    selectButton = [[UIButton alloc] initWithFrame:CGRectMake(selectButtonX, selectButtonY, selectButtonW, selectButtonH)];
    [selectButton setTitle:@"Select Drink" forState:UIControlStateNormal];
    [selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selectButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    selectButton.layer.cornerRadius = selectButtonH/8.0f;
    [selectButton addTarget:self action:@selector(selectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:selectButton ];
    
    yLocation += selectButtonH + yPadding;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE ADDBUTTON
    CGFloat addButtonW = width * .4f;
    CGFloat addButtonH = buttonHeight;
    CGFloat addButtonX = (width - addButtonW) * .5f;
    CGFloat addButtonY = yLocation;
    addButton = [[UIButton alloc] initWithFrame:CGRectMake(addButtonX, addButtonY, addButtonW, addButtonH)];
    [addButton setTitle:@"Add Drink" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    addButton.layer.cornerRadius = addButtonH/8.0f;
    [addButton addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:addButton ];
    
    yLocation += addButtonH + yPadding;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE SETTINGSBUTTON
    CGFloat settingsButtonW = width * .4f;
    CGFloat settingsButtonH = buttonHeight;
    CGFloat settingsButtonX = (width - settingsButtonW) * .5f;
    CGFloat settingsButtonY = yLocation;
    settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(settingsButtonX, settingsButtonY, settingsButtonW, settingsButtonH)];
    [settingsButton setTitle:@"Settings" forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingsButton setBackgroundColor:UIColorFromRGB(0x1886FB)];
    settingsButton.layer.cornerRadius = addButtonH/8.0f;
    [settingsButton addTarget:self action:@selector(settingsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:settingsButton ];
    
    yLocation += settingsButtonH + yPadding;
}

-(IBAction) selectButtonClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectAction)]) {
        [self.delegate selectAction];
    }
}

-(IBAction) addButtonClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addAction)]) {
        [self.delegate addAction];
    }
}

-(IBAction) settingsButtonClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(settingsAction)]) {
        [self.delegate settingsAction];
    }
}

@end
