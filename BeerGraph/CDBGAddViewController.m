//
//  CDBGAddViewController.m
//  BeerGraph
//
//  Created by Jonathan Hill on 8/25/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import "CDBGAddViewController.h"

@interface CDBGAddViewController ()

@end

@implementation CDBGAddViewController

@synthesize drinkManager;

-(void)viewDidLoad {
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = @"Add Drink";
    
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    
    ////////////////////////////////////////////////////////////////////////////////
    // DRAWS THE ADDVIEW
    CGFloat addViewX = 0;
    CGFloat addViewY = 0;
    CGFloat addViewW = width;
    CGFloat addViewH = height;
    addView= [[CDBGAddView alloc] initWithFrame:CGRectMake(addViewX, addViewY, addViewW, addViewH) drinkManager:drinkManager];
    addView.delegate = self;
    [self.view addSubview:addView ];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [addView registerForKeyboardNotifications];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [addView deregisterFromKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AddViewActionProtocol

-(void)createAction:(NSString *)drinkName drinkType:(NSNumber *)type calories:(NSNumber *)cal ounces:(NSNumber *)oz alcoholPct:(NSNumber *)alcPct {
    newDrinkName = drinkName;
    drinkType = type;
    calories = cal;
    ounces = oz;
    alcoholPct = alcPct;
    
    // Check to see if the drink already exists.
    CDBGDrink *existingDrink = [drinkManager getDrink:drinkName];
    if (existingDrink != nil) {
        // Drink already exists.  Check to see if it is a locked drink or not.
        if ([existingDrink.locked boolValue] == YES) {
            // Drink is locked.  Pop up a dialog and do not save the data.
            [self cancelDrink:drinkName];
            return;
        }
        else {
            // Drink is not locked.  Ask the user if they want to overwrite the data.
            [self overwriteDrink:drinkName];
            return;
        }
    }
    // Save the data
    [self.drinkManager saveDrink:drinkName
                             cal:cal
                              oz:oz
                             alc:alcPct
                             cat:type
                             fav:[NSNumber numberWithBool:NO]
                             ref:[NSNumber numberWithBool:NO]];
    CDBGDisplayViewController *displayViewController = [[CDBGDisplayViewController alloc] init];
    displayViewController.drinkManager = self.drinkManager;
    displayViewController.drinkName = drinkName;
    // locally store the navigation controller since
    // self.navigationController will be nil once we are popped
    UINavigationController *navController = self.navigationController;
    
    // Pop this controller and replace with another
    [navController popViewControllerAnimated:NO];
    [navController pushViewController:displayViewController animated:NO];
}

-(void)cancelAction {
    [self.navigationController  popToRootViewControllerAnimated:YES];
}

- (void)overwriteDrink:(NSString *)name {
    UIActionSheet *overwriteActionSheet = [[UIActionSheet alloc] initWithTitle: [[NSString alloc] initWithFormat:@"%@ already exists.  Update this drink with new information?", name] delegate:self                                                    cancelButtonTitle:nil destructiveButtonTitle:nil                                                otherButtonTitles:@"Yes", @"No", nil];
    [overwriteActionSheet setTag:1];
    overwriteActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [overwriteActionSheet showInView:self.view];
}

-(void)cancelDrink:(NSString *)name {
    UIActionSheet *cancelActionSheet = [[UIActionSheet alloc] initWithTitle: [[NSString alloc] initWithFormat:@"%@ is a predefined drink.  It cannot be edited.", name] delegate:self                                                    cancelButtonTitle:nil destructiveButtonTitle:nil                                                otherButtonTitles:@"OK", nil];
    [cancelActionSheet setTag:2];
    cancelActionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [cancelActionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case 1: {  // overwriteActionSheet
            if (buttonIndex == 0) {
                // yes button pressed
                // Save the data
                [self.drinkManager deleteDrink:newDrinkName];
                [self.drinkManager saveDrink:newDrinkName
                                         cal:calories
                                          oz:ounces
                                         alc:alcoholPct
                                         cat:drinkType
                                         fav:[NSNumber numberWithBool:NO]
                                         ref:[NSNumber numberWithBool:NO]];
                CDBGDisplayViewController *displayViewController = [[CDBGDisplayViewController alloc] init];
                displayViewController.drinkManager = self.drinkManager;
                displayViewController.drinkName = newDrinkName;
                // locally store the navigation controller since
                // self.navigationController will be nil once we are popped
                UINavigationController *navController = self.navigationController;
                
                // Pop this controller and replace with another
                [navController popViewControllerAnimated:NO];
                [navController pushViewController:displayViewController animated:NO];
            }
            else if (buttonIndex == 1) {
                // "no" button pressed
            }
        }
        case 2: {  // cancelActionSheet
        }
    }
}

@end
