//
//  CDBGSettingsView.h
//  BeerGraph
//
//  Created by Jonathan Hill on 8/24/15.
//  Copyright (c) 2015 Jonathan Hill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDBGDrinkManager.h"
#import "CDBGConstants.h"

@protocol SettingsViewActionProtocol <NSObject>
@required
-(void)drinkTypeSelectdAction:(NSNumber *)selection;
-(void)importDrinksAction;
-(void)deleteUserCreatedDrinksAction;
-(void)deleteAllDrinksAction;

@end

@interface CDBGSettingsView : UIView {
    UISegmentedControl* drinkTypeSegmentedControl;
    UIButton *importDrinksButton;
    UIButton *deleteUserCreatedDrinksButton;
    UIButton *deleteAllDrinksButton;
}

@property (nonatomic, weak) id<SettingsViewActionProtocol> delegate;
@property (nonatomic, strong) CDBGDrinkManager *drinkManager;

-(id)initWithFrame:(CGRect)frame;
-(id)initWithFrame:(CGRect)frame drinkManager:(CDBGDrinkManager *)dm;
-(void)drawView;
-(IBAction)segmentedControlValueDidChange:(UISegmentedControl *)segment;
-(IBAction)importDrinksButtonClicked:(id)sender;
-(IBAction)deleteUserDrinksClicked:(id)sender;
-(IBAction)deleteAllDrinksClicked:(id)sender;

@end
